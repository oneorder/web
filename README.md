# oneorder

This is the front-end repo for the oneorder app.

See npm scripts for more info on setup.

### Basic setup

Install Node from nodejs.org

```
npm install
npm run dev
```

### API setup

Get the oneorder-api repo and run it for full functionality.