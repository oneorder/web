const GET_ADDRESSES = 'redux/addresses/GET_ADDRESSES'
const GET_ADDRESSES_SUCCESS = 'redux/addresses/GET_ADDRESSES_SUCCESS'
const GET_ADDRESSES_FAIL = 'redux/addresses/GET_ADDRESSES_FAIL'

const initialState = {
  loaded: false
};

export default function reducer (state = initialState, action = {}) {
  switch (action.type) {
    case GET_ADDRESSES:
      return {
        ...state,
        loaded: false,
      }

    case GET_ADDRESSES_SUCCESS:
      return {
        ...state,
        ...action.result,
        loaded: true
      }

    default:
      return state;
  }
}

export function getAddresses ({ count, page }) {
  return {
    types: [GET_ADDRESSES, GET_ADDRESSES_SUCCESS, GET_ADDRESSES_FAIL],
    promise: (client) => client.get(`/address`, {
      query: { count, page }
    })
  };
}
