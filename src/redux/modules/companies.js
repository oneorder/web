const GET_COMPANIES = 'redux-example/companies/GET_COMPANIES'
const GET_COMPANIES_SUCCESS = 'redux-example/companies/GET_COMPANIES_SUCCESS'
const GET_COMPANIES_FAIL = 'redux-example/companies/GET_COMPANIES_FAIL'

const initialState = {
  loaded: false
};

export default function reducer (state = initialState, action = {}) {
  switch (action.type) {
    case GET_COMPANIES:
      return {
        ...state,
        loaded: false
      }

    case GET_COMPANIES_SUCCESS:
      return {
        ...state,
        data: action.result,
        loaded: true
      }

    default:
      return state;
  }
}

export function getCompanies ({ type }) {
  return {
    types: [GET_COMPANIES, GET_COMPANIES_SUCCESS, GET_COMPANIES_FAIL],
    promise: (client) => client.get('/company/', type ? { type } : {})
  };
}
