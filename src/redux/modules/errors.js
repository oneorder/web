import Alert from 'react-s-alert'

const { DISABLE_SENTRY } = process.env

const initialState = {
  loaded: false
};

export default function reducer (state = initialState, action = {}) {
  function error (message) {
    if (__CLIENT__) {
      Alert.error(message, {
        effect: 'slide'
      })
    }

    if (!DISABLE_SENTRY) {
      throw new Error(`Redux error: ${action.type}`)
    }
  }

  switch (action.type) {
    case 'redux/meals/POST_MEAL_FAILURE':
      if (action.error.name === 'ValidationError') {
        error(action.error.message)
      }
      return state

    case 'redux/auth/LOGIN_FAIL':
      error("Couldn't login. Double-check credentials.")
      return state

    case 'redux/orders/DELETE_ORDER_FAILURE':
      error('Order not deleted.')
      return state

    case 'redux/members/GET_MEMBERS_FAIL':
      error("Couldn't load members.")
      return state

    case 'redux/addresses/GET_ADDRESSES_FAIL':
      error("Couldn't load addresses.")
      return state

    case 'redux/address/PATCH_ADDRESS_FAIL':
      error("Couldn't update member info.")
      return state

    case 'redux/meals/GET_MEAL_FAILURE':
      error("Couldn't load meal details.")
      return state

    case 'redux/orders/GET_ORDERS_FAILURE':
      error("Couldn't load orders.")
      return state

    default:
      return state
  }
}
