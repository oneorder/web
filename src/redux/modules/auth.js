import Raven from 'raven-js'

const LOGIN = 'redux/auth/LOGIN'
const LOGIN_SUCCESS = 'redux/auth/LOGIN_SUCCESS'
const LOGIN_FAIL = 'redux/auth/LOGIN_FAIL'
const REGISTER = 'redux/auth/REGISTER'
const REGISTER_SUCCESS = 'redux/auth/REGISTER_SUCCESS'
const REGISTER_FAILURE = 'redux/auth/REGISTER_FAILURE'
const LOGOUT = 'redux/auth/LOGOUT'
const LOGOUT_SUCCESS = 'redux/auth/LOGOUT_SUCCESS'
const LOGOUT_FAILURE = 'redux/auth/LOGOUT_FAILURE'
const GET_ME = 'redux/auth/GET_ME'
const GET_ME_SUCCESS = 'redux/auth/GET_ME_SUCCESS'
const GET_ME_FAILURE = 'redux/auth/GET_ME_FAILURE'

const initialState = {
  loaded: false
}

const initRavenSession = (action = null) => {
  if (__CLIENT__) {
    const data = action ?
      {
        email: action.result.data.email,
        id: action.result.data._key
      }
    :
      undefined

    Raven.setUserContext(data)
  }
}

export default function reducer (state = initialState, action = {}) {
  switch (action.type) {
    case LOGIN:
      return {
        ...state,
        loggingIn: true
      };
    case LOGIN_SUCCESS:
      if (__CLIENT__ && !window.localStorage.authToken) {
        window.localStorage.authToken = action.result.data.authToken
      }
      initRavenSession(action)

      return {
        ...state,
        loggingIn: false,
        user: action.result.data
      };

    case LOGIN_FAIL:
      return {
        ...state,
        loggingIn: false,
        user: null,
        loginError: action.error
      };

    case GET_ME_SUCCESS:
      initRavenSession(action)
      return {
        ...state,
        user: action.result.data
      }
    case REGISTER_SUCCESS:
      if (__CLIENT__) {
        window.localStorage.authToken = action.result.data.authToken
      }
      initRavenSession(action)
      return {
        ...state,
        user: action.result.data,
        loaded: true
      }

    case LOGOUT_SUCCESS:
      if (__CLIENT__) {
        initRavenSession()
        delete window.localStorage.authToken
      }
      return {
        ...state,
        user: null
      }

    default:
      return state;
  }
}

export function isLoaded (globalState) {
  return globalState.auth && globalState.auth.loaded;
}

export function login (credentials) {
  return {
    types: [LOGIN, LOGIN_SUCCESS, LOGIN_FAIL],
    promise: (client) => client.post('/session', {
      data: credentials
    })
  };
}

export function register (credentials) {
  return {
    types: [REGISTER, REGISTER_SUCCESS, REGISTER_FAILURE],
    promise: (client) => client.post('/user', {
      data: credentials
    })
  }
}

export function getMe () {
  return {
    types: [GET_ME, GET_ME_SUCCESS, GET_ME_FAILURE],
    promise: (client) => client.get('/user/me')
  }
}

export function logout () {
  return {
    types: [LOGOUT, LOGOUT_SUCCESS, LOGOUT_FAILURE],
    promise: () => (Promise.resolve())
  }
}
