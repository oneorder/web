const GET_ADDRESS = 'redux/address/GET_ADDRESS'
const GET_ADDRESS_SUCCESS = 'redux/address/GET_ADDRESS_SUCCESS'
const GET_ADDRESS_FAIL = 'redux/address/GET_ADDRESS_FAIL'
const PATCH_ADDRESS = 'redux/address/PATCH_ADDRESS'
const PATCH_ADDRESS_SUCCESS = 'redux/address/PATCH_ADDRESS_SUCCESS'
const PATCH_ADDRESS_FAIL = 'redux/address/PATCH_ADDRESS_FAIL'

const initialState = {
  loaded: false
};

export default function reducer (state = initialState, action = {}) {
  switch (action.type) {
    case GET_ADDRESS:
      return {
        ...state,
        loaded: false,
      }

    case GET_ADDRESS_SUCCESS:
      return {
        ...state,
        data: action.result.data.defaultAddress,
        loaded: true
      }

    case PATCH_ADDRESS:
      return {
        ...state,
        loaded: false
      }

    case PATCH_ADDRESS_SUCCESS:
      return {
        ...state,
        ...action.result,
        loaded: false
      }

    default:
      return state;
  }
}

export function getAddress ({ user }) {
  return {
    types: [GET_ADDRESS, GET_ADDRESS_SUCCESS, GET_ADDRESS_FAIL],
    promise: (client) => client.get(`/user/${user}`)
  };
}

export function patchAddress ({ fields, user, address } = {}) {
  return {
    types: [PATCH_ADDRESS, PATCH_ADDRESS_SUCCESS, PATCH_ADDRESS_FAIL],
    promise: (client) => client.patch(`/user/${user}/address/${address}`, {
      data: fields
    })
  }
}
