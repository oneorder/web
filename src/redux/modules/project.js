const POST_PROJECT = 'redux-example/project/POST_PROJECT'
const POST_PROJECT_SUCCESS = 'redux-example/project/POST_PROJECT_SUCCESS'
const POST_PROJECT_FAIL = 'redux-example/project/POST_PROJECT_FAIL'
const GET_PROJECT = 'redux-example/project/GET_PROJECT'
const GET_PROJECT_SUCCESS = 'redux-example/project/GET_PROJECT_SUCCESS'
const GET_PROJECT_FAIL = 'redux-example/project/GET_PROJECT_FAIL'

const initialState = {
  loaded: false
};

export default function reducer (state = initialState, action = {}) {
  switch (action.type) {
    case POST_PROJECT:
      return {
        ...state,
        loaded: false
      }

    case POST_PROJECT_SUCCESS:
      return {
        ...state,
        data: action.result,
        loaded: true
      }

    case GET_PROJECT:
      return {
        ...state,
        loaded: false,
      }

    case GET_PROJECT_SUCCESS:
      return {
        ...state,
        data: action.result,
        loaded: true
      }

    default:
      return state;
  }
}

export function postProject (data) {
  return {
    types: [POST_PROJECT, POST_PROJECT_SUCCESS, POST_PROJECT_FAIL],
    promise: (client) => client.post('/project', {
      data: {
        ...data,
        user: 1
      }
    })
  }
}

export function getProject (id) {
  return {
    types: [GET_PROJECT, GET_PROJECT_SUCCESS, GET_PROJECT_FAIL],
    promise: (client) => client.get(`/project/${id}`)
  };
}
