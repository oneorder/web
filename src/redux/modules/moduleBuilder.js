import { capitalize } from 'lodash'

const initialState = {
  loaded: false
}

export class moduleBuilder {
  constructor (args = {}) {
    const { path } = args
    this.asset = path[path.length - 1]
    this.ASSET = this.asset.toUpperCase()

    this.ASSET_GET = `modules/${this.asset}/${this.ASSET}_GET`
    this.ASSET_GET_SUCCESS = `modules/${this.asset}/${this.ASSET}_GET_SUCCESS`
    this.ASSET_GET_FAILURE = `modules/${this.asset}/${this.ASSET}_GET_FAILURE`
    this.ASSET_DELETE = `modules/${this.asset}/${this.ASSET}_DELETE`
    this.ASSET_DELETE_SUCCESS = `modules/${this.asset}/${this.ASSET}_DELETE_SUCCESS`
    this.ASSET_DELETE_FAILURE = `modules/${this.asset}/${this.ASSET}_DELETE_FAILURE`

    this[`delete${capitalize(this.asset)}`] = (props) => {
      const endpoint = `${path.map(pth => (`${pth}/${props[pth]}`))}`.toString().replace(',', '/')

      return {
        types: [this.ASSET_DELETE, this.ASSET_DELETE_SUCCESS, this.ASSET_DELETE_FAILURE],
        promise: (client) => client.del(endpoint)
      }
    }

    this[`get${capitalize(this.asset)}`] = (props) => {
      const endpoint = `${path.map(pth => (`${pth}/${props[pth]}`))}`.toString().replace(',', '/')

      return {
        types: [this.ASSET_GET, this.ASSET_GET_SUCCESS, this.ASSET_GET_FAILURE],
        promise: (client) => client.get(endpoint)
      }
    }
  }

  reducer (state = initialState, action = {}) {
    switch (action.type) {
      case this.GET_ASSET:
        return {
          ...initialState,
          loaded: false
        }

      case this.GET_ASSET_SUCCESS:
        return {
          ...state,
          ...action.result,
          loaded: true
        }

      case this.ASSET_DELETE_SUCCESS:
        return {
          ...state,
          loaded: true
        }

      case this.ASSET_DELETE:
        return {
          ...state,
          loaded: true
        }

      default:
        return state;
    }
  }
}
