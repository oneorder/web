const POST_ORDERS = 'redux/orders/POST_ORDERS'
const POST_ORDERS_SUCCESS = 'redux/orders/POST_ORDERS_SUCCESS'
const POST_ORDERS_FAILURE = 'redux/orders/POST_ORDERS_FAILURE'
const GET_ORDERS = 'redux/orders/GET_ORDERS'
const GET_ORDERS_SUCCESS = 'redux/orders/GET_ORDERS_SUCCESS'
const GET_ORDERS_FAILURE = 'redux/orders/GET_ORDERS_FAILURE'
const DELETE_ORDER = 'redux/orders/DELETE_ORDER'
const DELETE_ORDER_SUCCESS = 'redux/orders/DELETE_ORDER_SUCCESS'
const DELETE_ORDER_FAILURE = 'redux/orders/DELETE_ORDER_FAILURE'

const initialState = {
  loaded: false,
  submitting: false,
  error: undefined
};

export default function reducer (state = initialState, action = {}) {
  switch (action.type) {
    case POST_ORDERS:
      return {
        ...state,
        loaded: false,
        submitting: true,
        error: undefined
      }

    case POST_ORDERS_SUCCESS:
      return {
        ...state,
        loaded: true,
        submitting: false
      }

    case POST_ORDERS_FAILURE:
      return {
        ...state,
        loaded: true,
        submitting: false,
        error: action.error
      }

    case GET_ORDERS:
      return {
        ...initialState,
        loaded: false,
        error: undefined
      }

    case GET_ORDERS_SUCCESS:
      return {
        ...state,
        ...action.result,
        loaded: true
      }

    case GET_ORDERS_FAILURE:
      return {
        ...state,
        ...action.result,
        loaded: false,
        error: action.error
      }

    default:
      return state;
  }
}

export function postOrders ({ user, orders }) {
  return {
    types: [POST_ORDERS, POST_ORDERS_SUCCESS, POST_ORDERS_FAILURE],
    promise: (client) => client.post(`/user/${user}/orders`, {
      data: {
        orders
      }
    })
  }
}

export function getOrders ({ start, end } = {}) {
  return {
    types: [GET_ORDERS, GET_ORDERS_SUCCESS, GET_ORDERS_FAILURE],
    promise: (client) => client.get(`/orders`, {
      query: {
        start: start || new Date(),
        end: end || new Date()
      }
    })
  }
}

export function deleteOrder (key) {
  return {
    types: [DELETE_ORDER, DELETE_ORDER_SUCCESS, DELETE_ORDER_FAILURE],
    promise: (client) => client.del(`/orders/${key}`)
  }
}
