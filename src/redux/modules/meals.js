const GET_MEALS = 'redux/meals/GET_MEALS'
const GET_MEALS_SUCCESS = 'redux/meals/GET_MEALS_SUCCESS'
const GET_MEALS_FAIL = 'redux/meals/GET_MEALS_FAIL'
const POST_MEAL = 'redux/meals/POST_MEAL'
const POST_MEAL_SUCCESS = 'redux/meals/POST_MEAL_SUCCESS'
const POST_MEAL_FAILURE = 'redux/meals/POST_MEAL_FAILURE'

const initialState = {
  loaded: false
};

export default function reducer (state = initialState, action = {}) {
  switch (action.type) {
    case GET_MEALS:
      return {
        ...initialState,
        loaded: false,
      }

    case POST_MEAL_SUCCESS:
      const newData = state.data.slice()
      newData.unshift(action.result.data)

      return {
        ...state,
        data: newData,
        loaded: true
      }

    case POST_MEAL:
      return {
        ...state,
        loaded: false
      }

    case GET_MEALS_SUCCESS:
      return {
        ...state,
        ...action.result,
        loaded: true
      }

    default:
      return state;
  }
}

export function getMeals ({ count, page, start, end }) {
  return {
    types: [GET_MEALS, GET_MEALS_SUCCESS, GET_MEALS_FAIL],
    promise: (client) => client.get(`/meal`, {
      query: {
        count,
        page,
        start,
        end
      }
    })
  };
}

export function postMeal (data) {
  return {
    types: [POST_MEAL, POST_MEAL_SUCCESS, POST_MEAL_FAILURE],
    promise: (client) => client.post('/meal', {
      data
    })
  }
}
