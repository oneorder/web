const GET_MEMBERS = 'redux/members/GET_MEMBERS'
const GET_MEMBERS_SUCCESS = 'redux/members/GET_MEMBERS_SUCCESS'
const GET_MEMBERS_FAIL = 'redux/members/GET_MEMBERS_FAIL'
const POST_MEMBER = 'redux/members/POST_MEMBER'
const POST_MEMBER_SUCCESS = 'redux/members/POST_MEMBER_SUCCESS'
const POST_MEMBER_FAILURE = 'redux/members/POST_MEMBER_FAILURE'
const DELETE_MEMBER = 'redux/members/DELETE_MEMBER'
const DELETE_MEMBER_SUCCESS = 'redux/members/DELETE_MEMBER_SUCCESS'
const DELETE_MEMBER_FAILURE = 'redux/members/DELETE_MEMBER_FAILURE'
const PATCH_MEMBER = 'redux/members/PATCH_MEMBER'
const PATCH_MEMBER_SUCCESS = 'redux/members/PATCH_MEMBER_SUCCESS'
const PATCH_MEMBER_FAILURE = 'redux/members/PATCH_MEMBER_FAILURE'

const initialState = {
  loaded: false
};

export default function reducer (state = initialState, action = {}) {
  switch (action.type) {
    case GET_MEMBERS:
      return {
        ...state,
        loaded: false,
      }

    case POST_MEMBER_SUCCESS:
      const newData = state.data.slice()
      newData.unshift(action.result.data)

      return {
        ...state,
        data: newData,
        loaded: true
      }

    case POST_MEMBER:
      return {
        ...state,
        loaded: false
      }

    case GET_MEMBERS_SUCCESS:
      return {
        ...state,
        ...action.result,
        loaded: true
      }

    default:
      return state;
  }
}

export function getMembers ({ count, page } = {}) {
  return {
    types: [GET_MEMBERS, GET_MEMBERS_SUCCESS, GET_MEMBERS_FAIL],
    promise: (client) => client.get(`/user`, {
      query: {
        count: count || 100,
        page: page || 1
      }
    })
  };
}

export function postMember (credentials) {
  return {
    types: [POST_MEMBER, POST_MEMBER_SUCCESS, POST_MEMBER_FAILURE],
    promise: (client) => client.post('/user', {
      data: credentials
    })
  }
}

export function deleteMember (key) {
  return {
    types: [DELETE_MEMBER, DELETE_MEMBER_SUCCESS, DELETE_MEMBER_FAILURE],
    promise: (client) => client.del(`/user/${key}`)
  }
}

export function patchMember ({ data, _key } = {}) {
  return {
    types: [PATCH_MEMBER, PATCH_MEMBER_SUCCESS, PATCH_MEMBER_FAILURE],
    promise: (client) => client.patch(`/user/${_key}`, {
      data: data
    })
  }
}
