const GET_MEAL = 'redux/meals/GET_MEAL'
const GET_MEAL_SUCCESS = 'redux/meals/GET_MEAL_SUCCESS'
const GET_MEAL_FAILURE = 'redux/meals/GET_MEAL_FAILURE'
const DELETE_MEAL = 'redux/meals/DELETE_MEAL'
const DELETE_MEAL_SUCCESS = 'redux/meals/DELETE_MEAL_SUCCESS'
const DELETE_MEAL_FAILURE = 'redux/meals/DELETE_MEAL_FAILURE'
const PATCH_MEAL = 'redux/meals/PATCH_MEAL'
const PATCH_MEAL_SUCCESS = 'redux/meals/PATCH_MEAL_SUCCESS'
const PATCH_MEAL_FAILURE = 'redux/meals/PATCH_MEAL_FAILURE'
const CLEAR = 'redux/meals/CLEAR'
const CLEAR_SUCCESS = 'redux/meals/CLEAR_SUCCESS'
// const PATCH_MEAL_FAILURE = 'redux/meals/PATCH_MEAL_FAILURE'

const initialState = {
  loaded: false
};

export default function reducer (state = initialState, action = {}) {
  switch (action.type) {
    case CLEAR_SUCCESS:
      return {
        ...initialState,
        loaded: true
      }

    case GET_MEAL:
      return {
        ...state,
        loaded: false
      }

    case GET_MEAL_SUCCESS:
      return {
        ...state,
        ...action.result,
        loaded: true
      }

    case DELETE_MEAL:
      return {
        ...state,
        loaded: false
      }

    case DELETE_MEAL_SUCCESS:
      return {
        ...state,
        loaded: true
      }

    case PATCH_MEAL:
      return {
        ...state,
        loaded: false
      }

    case PATCH_MEAL_SUCCESS:
      return {
        ...state,
        ...action.result,
        loaded: true
      }

    default:
      return state;
  }
}

export function clear () {
  return {
    types: [CLEAR, CLEAR_SUCCESS],
    promise: () => Promise.resolve({})
  }
}

export function getMeal (key) {
  return {
    types: [GET_MEAL, GET_MEAL_SUCCESS, GET_MEAL_FAILURE],
    promise: (client) => client.get(`/meal/${key}`)
  }
}

export function deleteMeal (key) {
  return {
    types: [DELETE_MEAL, DELETE_MEAL_SUCCESS, DELETE_MEAL_FAILURE],
    promise: (client) => client.del(`/meal/${key}`)
  }
}

export function patchMeal ({ _key, data }) {
  return {
    types: [PATCH_MEAL, PATCH_MEAL_SUCCESS, PATCH_MEAL_FAILURE],
    promise: (client) => client.patch(`/meal/${_key}`, {
      data
    })
  }
}


