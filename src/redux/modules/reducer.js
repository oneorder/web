import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux'
import {reducer as reduxAsyncConnect} from 'redux-connect'
import { reducer as formReducer } from 'redux-form'

import auth from './auth'
import companies from './companies'
import company from './company'
import project from './project'
import addresses from './addresses'
import address from './address'
import meals from './meals'
import meal from './meal'
import members from './members'
import orders from './orders'
import errors from './errors'
import success from './success'

export default combineReducers({
  address,
  addresses,
  auth,
  companies,
  company,
  form: formReducer,
  meal,
  meals,
  members,
  orders,
  project,
  reduxAsyncConnect,
  routing: routerReducer,
  success,
  errors
})
