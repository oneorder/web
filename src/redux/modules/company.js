const POST_COMPANY = 'redux-example/company/POST_COMPANY'
const POST_COMPANY_SUCCESS = 'redux-example/company/POST_COMPANY_SUCCESS'
const POST_COMPANY_FAIL = 'redux-example/company/POST_COMPANY_FAIL'
const GET_COMPANY = 'redux-example/company/GET_COMPANY'
const GET_COMPANY_SUCCESS = 'redux-example/company/GET_COMPANY_SUCCESS'
const GET_COMPANY_FAIL = 'redux-example/company/GET_COMPANY_FAIL'

const initialState = {
  loaded: false
};

export default function reducer (state = initialState, action = {}) {
  switch (action.type) {
    case POST_COMPANY:
      return {
        ...state,
        loaded: false
      }

    case POST_COMPANY_SUCCESS:
      return {
        ...state,
        data: action.result,
        loaded: true
      }

    case GET_COMPANY:
      return {
        ...state,
        loaded: false,
      }

    case GET_COMPANY_SUCCESS:
      return {
        ...state,
        data: action.result,
        loaded: true
      }

    default:
      return state;
  }
}

export function postCompany (data) {
  return {
    types: [POST_COMPANY, POST_COMPANY_SUCCESS, POST_COMPANY_FAIL],
    promise: (client) => client.post('/company', {
      data
    })
  };
}

export function getCompany (id) {
  return {
    types: [GET_COMPANY, GET_COMPANY_SUCCESS, GET_COMPANY_FAIL],
    promise: (client) => client.get(`/company/${id}`)
  };
}
