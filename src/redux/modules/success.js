import Alert from 'react-s-alert'

const initialState = {
  loaded: false
};

const success = (message) => {
  if (__CLIENT__) {
    Alert.success(message, {
      effect: 'slide'
    })
  }
}

export default function reducer (state = initialState, action = {}) {
  switch (action.type) {
    case 'redux/meals/POST_MEAL_SUCCESS':
      success('Meal added.')
      break

    case 'redux/auth/LOGIN_SUCCESS':
      success('Logged in. Welcome!')
      break

    case 'redux/members/PATCH_MEMBER_SUCCESS':
      success('Personal info updated.')
      break

    case 'redux/orders/POST_ORDERS_SUCCESS':
      success('Orders on the way!')
      break

    case 'redux/address/PATCH_ADDRESS_SUCCESS':
      success('Address info updated.')
      break

    case 'redux/members/DELETE_MEMBER_SUCCESS':
      success('Member deleted.')
      break

    case 'redux/meals/DELETE_MEAL_SUCCESS':
      success('Meal deleted.')
      break

    case 'redux/orders/DELETE_ORDER_SUCCESS':
      success('Order deleted.')
      break

    case 'redux/auth/LOGOUT_SUCCESS':
      success('You were logged out. Bye for now.')
      break

    default:
      break
  }

  return state
}
