import React, {Component} from 'react'
import {PropTypes} from 'prop-types'
import { autobind } from 'core-decorators'
import { Field } from 'redux-form'
import moment from 'moment'
import { capitalize } from 'lodash'

import { groupBy, sortBy } from 'lodash'
import { Radio } from './Fields'

import veganIcon from '../../static/images/icSpa.png'
import meatIcon from '../../static/images/icLocalDining.png'
import customIcon from '../../static/images/icBrightness.png'

export default class OrderField extends Component {
  static propTypes = {
    children: PropTypes.element,
    meta: PropTypes.object,
    meals: PropTypes.array,
    label: PropTypes.string,
    name: PropTypes.string
  }

  constructor (props) {
    super(props)
    this.state = {
      showCustom: false
    }
  }

  @autobind
  customClickHandler (event) {
    if (event.target.value !== 'meat' && event.target.value !== 'vegan') {
      this.setState({ showCustom: true })
    } else {
      this.setState({ showCustom: false })
    }
  }

  render () {
    const { meals, label, name } = this.props
    const { showCustom } = this.state
    const day = moment(name).startOf('day').format('YYYY-MM-DD')

    let mealGroups = sortBy(meals, 'diet').reverse()
    mealGroups = groupBy(mealGroups, 'meal')

    const renderMeal = (meal) => {
      const options = mealGroups[meal].map((dish) => ({
        label: dish.name,
        value: dish._key,
        icon: dish.diet === 'vegan' ? veganIcon : meatIcon
      }))

      return (
        <Field
          label={capitalize(meal)}
          name={`${day}.${meal}`}
          component={Radio}
          options={options}
          style={{
            flexDirection: 'column',
            margin: 3,
            height: 61,
            border: 'solid 1px black',
            justifyContent: 'space-around'
          }}
          labelStyle={{
            marginTop: 5,
            fontFamily: 'Avenir Next Regular'
          }}
        />
      )
    }

    return (
      <div>
        <Field
          name={day}
          label={label.toUpperCase()}
          onChange={this.customClickHandler}
          component={Radio}
          options={[
            { label: 'Full day vegan', value: 'vegan', icon: veganIcon },
            { label: 'Full day regular', value: 'meat', icon: meatIcon },
            { label: 'Custom', value: 'custom', icon: customIcon }
          ]}
          labelStyle={{
            fontFamily: 'Avenir Next Regular',
            fontSize: 15,
            paddingTop: 10
          }}
        />
        {showCustom &&
          <div style={{paddingLeft: 10, paddingRight: 10}}>
            { mealGroups.breakfast && renderMeal('breakfast') }
            { mealGroups.lunch && renderMeal('lunch') }
            { mealGroups.dinner && renderMeal('dinner') }
          </div>
        }
      </div>
    )
  }
}
