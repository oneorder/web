import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import Personal from './Profile-Personal'
import Address from './Profile-Address'
import Title from 'components/Title'
import { Flex, Box } from 'grid-styled'

@connect(
  (state) => ({
    user: state.auth.user
  })
)
export default class Profile extends Component {
  static propTypes = {
    location: PropTypes.object,
    user: PropTypes.object
  }

  render () {
    const { location } = this.props

    if (!this.props.user || location.query.member) {
      return null
    }

    return (
      <Flex
        width={1}
        align="center"
        justify="center"
      >
        <Box
          width={[1, 1, 2 / 3, 830]}
          align="center"
          justify="center"
          px={2}
          style={{
            backgroundColor: 'white'
          }}
        >
          <Flex
            width={1}
            justify="center"
            px={2}
          >
            <Title />
          </Flex>
          <Personal location={location} />
          <Address location={location} />
        </Box>
      </Flex>
    )
  }
}
