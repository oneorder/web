import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {browserHistory} from 'react-router'
import {reduxForm, Field, SubmissionError} from 'redux-form'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import {autobind} from 'core-decorators'
import { Flex, Box } from 'grid-styled'

import { register, login } from '_redux/modules/auth'

import Register from './Register'
import Title from './Title'
import Button from 'components/common/Button'
import { TextField, validate } from './Fields'

import classes from './Login.scss'
// const classes = {}

@connect(
  () => ({}),
  (dispatch) => bindActionCreators({
    login,
    register
  }, dispatch)
)
@reduxForm({
  form: 'login'
})
export default class Login extends Component {
  static propTypes = {
    register: PropTypes.func,
    login: PropTypes.func,
    onSubmit: PropTypes.func,
    fields: PropTypes.object,
    handleSubmit: PropTypes.func,
    onToggleRegistering: PropTypes.func,
    registering: PropTypes.bool,
    hideLogin: PropTypes.bool,
    location: PropTypes.object,
    dismiss: PropTypes.func,
    error: PropTypes.string,
    submitting: PropTypes.bool,
    pristine: PropTypes.bool
  }

  @autobind
  registerClickHandler () {
    const { registering } = this.props

    browserHistory.push({ query:
      registering ? { login: true } : { register: true }
    })
  }

  @autobind
  cancelHandler () {
    const { location, dismiss } = this.props

    if (dismiss) {
      return dismiss()
    }

    browserHistory.push({
      ...location,
      query: { register: undefined, login: undefined }
    })
  }

  @autobind
  submitHandler (fields) {
    const {registering, onSubmit, dismiss} = this.props

    if (onSubmit) {
      onSubmit(fields)
      .then(() => {
        return this.props.dismiss ? this.props.dismiss() : this.cancelHandler()
      })

      return
    }

    if (registering) {
      return this.props.register({ // eslint-disable-line consistent-return
        ...fields
      })
      .then(() => {
        return dismiss()
      })
      .catch((error) => {
        throw new SubmissionError(error.fields || { _error: error })
      })
    }

    return this.props.login(fields) // eslint-disable-line consistent-return
    .then(() => {
      return dismiss()
    })
    .catch(() => {
      throw new SubmissionError({ _error: 'Wrong email or password. Try again.' })
    })
  }

  render () {
    const {handleSubmit, registering, hideLogin, submitting, pristine} = this.props

    const DialogButtons = () => (
      <Flex justify="center" align="center" my={2}>
        { !hideLogin &&
          <input
            type="button"
            className={classes.registerToggle}
            onClick={this.registerClickHandler}
            value={ !registering ? 'Register' : 'Login' }
          />
        }
        <Button
          primary
          type="submit"
          className={ classes.submit }
          onClick={handleSubmit(this.submitHandler)}
          disabled={submitting || pristine}
        >
          SIGN IN
        </Button>
      </Flex>
    )

    if (!registering) {
      return (
        <div
          id="Login"
        >
          <div style={{
            display: 'flex',
            justifyContent: 'center'
          }}>
            <Title />
          </div>
          <h1>{ !registering ? 'Login' : 'Register' }</h1>
          <form onSubmit={handleSubmit(this.submitHandler)}>
            <Field
              id="Login_email"
              name="email"
              label="Email"
              type="email"
              component={TextField}
              validate={[validate.isRequired, validate.isEmail]}
            />
            <Field
              id="Login_password"
              name="password"
              label="Password"
              type="password"
              component={TextField}
              validate={validate.isRequired}
            />
            {this.props.error &&
              <Flex pt={2} justify="center" align="center">
                <Box>{this.props.error}</Box>
              </Flex>
            }
            <DialogButtons />
          </form>
        </div>
      )
    }

    return (
      <div
        id="Login"
      >
        <Register />
        <DialogButtons
          registering={registering}
        />
      </div>
    )
  }
}
