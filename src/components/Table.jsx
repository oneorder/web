import React from 'react'
import { capitalize } from 'lodash'

import { Row } from './Flexbox'

const Table = ({ columns, data }) => (
  <div>
    <Row
      style={{
        justifyContent: 'space-between',
        alignItems: 'center',
        padding: '0 16px',
        height: 32
      }}
    >
      {columns.map((field, ind) => (
        <label
          key={ind}
          style={{
            width: `${1 / columns.length * 100}%`
          }}
        >
          {capitalize(field)}
        </label>
      ))}
    </Row>
    {data && data.map((row, index) => (
      <Row
        key={index}
        style={{
          justifyContent: 'space-between',
          alignItems: 'center',
          padding: '0 16px',
          height: 32,
          border: '1px solid black',
          marginBottom: 10
        }}
      >
        {columns.map((field, ind) => {
          if (row[field]) {
            return (
              <div
                key={ind}
                style={{
                  width: `${1 / columns.length * 100}%`
                }}
              >
                {row[field]}
              </div>
            )
          }
        })}
      </Row>
    ))}
  </div>
)

export default Table
