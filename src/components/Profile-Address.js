import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { reduxForm } from 'redux-form'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { autobind } from 'core-decorators'
import { patchAddress, getAddress } from '_redux/modules/address'
import { getMe } from '_redux/modules/auth'

import Address from './Register-Address'
import Button from 'components/common/Button'
import { Column } from 'components/common/Flexbox'

import submitIcon from '../../static/images/icGrain.png'

@connect(
  (state) => ({
    user: state.auth.user,
    address: state.address.data,
    initialValues: {
      address: state.address.data
    }
  }),
  (dispatch) => bindActionCreators({
    patchAddress,
    getAddress,
    getMe
  }, dispatch)
)
@reduxForm({
  form: 'profileAddress',
  enableReinitialize: true
})
export default class Profile extends Component {
  static propTypes = {
    location: PropTypes.object,
    handleSubmit: PropTypes.func,
    patchAddress: PropTypes.func,
    getAddress: PropTypes.func,
    initialValues: PropTypes.object,
    user: PropTypes.object,
    initialize: PropTypes.func,
    getMe: PropTypes.func,
    onComplete: PropTypes.func,
    address: PropTypes.object,
    submitting: PropTypes.bool,
    pristine: PropTypes.bool
  }

  componentDidMount () {
    const { location: { query } } = this.props

    return this.props.getAddress({
      user: query.member || this.props.user._key
    })
  }

  @autobind
  patchAddressHandler ({ address: fields }) {
    const { location: { query } } = this.props
    const user = query.member
    const address = this.props.address._key

    if (query.member && this.props.user.role === 'ADMIN') {
      return this.props.patchAddress({
        user,
        fields,
        address
      })
      .then(() => {
        if (this.props.onComplete) {
          return this.props.onComplete()
        }
      })
    }

    return this.props.patchAddress({
      user,
      fields,
      address
    })
    .then(() => {
      this.props.getAddress()

      if (this.props.onComplete) {
        return this.props.onComplete()
      }
    })
  }

  render () {
    const { handleSubmit, submitting, pristine } = this.props

    return (
      <form
        style={{
          width: '100%'
        }}
        onSubmit={handleSubmit(this.patchAddressHandler)}
      >
        <h1>Update address</h1>
        <Address />
        <Column
          style={{
            alignItems: 'center',
            marginTop: 30
          }}
        >
          <Button
            icon={submitIcon}
            disabled={submitting || pristine}
            style={{
              backgroundColor: '#333333',
              color: '#fff',
              width: '221px'
            }}
          >
            Submit changes
          </Button>
        </Column>
      </form>
    )
  }
}
