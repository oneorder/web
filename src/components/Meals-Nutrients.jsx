import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Field, formValueSelector } from 'redux-form'
import { connect } from 'react-redux'

import { TextField, Select } from './Fields'
import { Row } from './Flexbox'
import Add from './Meals-Nutrients-Add'

const selector = formValueSelector('meal')

@connect(
  (state) => ({
    nutrients: selector(state, 'nutrients')
  })
)
export default class Nutrients extends Component {
  static propTypes = {
    fields: PropTypes.array,
    nutrients: PropTypes.array
  }

  static defaultProps = {
    nutrients: [{}]
  }

  render () {
    const { fields, nutrients } = this.props

    const nutes = [
      { name: 'carbs', unit: 'grams', color: 'black' },
      { name: 'fat', unit: 'grams', color: 'black' },
      { name: 'sat. fat', unit: 'grams', color: 'black' },
      { name: 'trans fat', unit: 'grams', color: 'black' },
      { name: 'protein', unit: 'grams', color: 'black' },
      { name: 'added sugar', unit: 'grams', color: 'black' },
      { name: 'magnesium', unit: 'milligrams', color: 'white' },
      { name: 'zinc', unit: 'milligrams', color: 'white' },
      { name: 'fibre', unit: 'grams', color: 'white' },
      { name: 'calcium', unit: 'milligrams', color: 'white' },
      { name: 'iron', unit: 'milligrams', color: 'white' },
      { name: 'potassium', unit: 'milligrams', color: 'white' }
    ]

    const names = nutes.map((nt) => nt.name)
    const completed = nutrients && nutrients.map((nt) => nt.name)

    return (
      <div>
        <label>Nutrients</label>
        {fields && fields.map((nutrient, index) => (
          <Row key={index}>
            <Field
              type="text"
              label="Name"
              name={`${nutrient}.name`}
              component={Select}
              options={names}
              capitalize
            />
            <Field
              label="Unit"
              name={`${nutrient}.unit`}
              options={['%', 'grams']}
              component={Select}
            />
            <Field
              type="number"
              label="Quantity"
              name={`${nutrient}.quantity`}
              component={TextField}
              parse={Number}
            />
          </Row>
        ))}
        <h4>Click nutrient to add</h4>
        <Row
          style={{
            cursor: 'pointer',
            flexWrap: 'wrap'
          }}
        >
          {nutes.map((nute, index) => {
            if (!completed.includes(nute.name)) {
              return (
                <Add
                  key={index}
                  push={fields.push}
                  nutrient={nute}
                />
              )
            }
          })}
        </Row>
      </div>
    )
  }
}
