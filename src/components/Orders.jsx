import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { bindActionCreators } from 'redux'
import { asyncConnect } from 'redux-connect'
import { Link, browserHistory } from 'react-router'
import { autobind } from 'core-decorators'
import moment from 'moment'
import { isEmpty } from 'lodash'
import { getMeals } from '_redux/modules/meals'
import { getOrders } from '_redux/modules/orders'

import { Row } from './Flexbox'
import Window from './Window'
import AddOrders from './Orders-New'
import DateChanger from './common/DateChanger'

@asyncConnect([{
  promise: ({ store: { getState, dispatch }, location }) => {
    if (!getState().orders.loaded) {
      return dispatch(getOrders({
        start: location.query.date,
        end: location.query.date
      }))
    }
  }
}, {
  promise: ({ store: { getState, dispatch }, location }) => {
    const { count, page } = location.query

    if (!getState().meals.loaded) {
      return dispatch(getMeals({
        count,
        page
      }))
    }
  }
}],
  (state) => ({
    orders: state.orders.data,
    meta: state.orders.meta,
    loaded: state.orders.loaded
  }),
  (dispatch) => bindActionCreators({
    getMeals,
    getOrders
  }, dispatch)
)
export default class Orders extends Component {
  static propTypes = {
    location: PropTypes.object,
    handleSubmit: PropTypes.func,
    meals: PropTypes.array,
    getMeals: PropTypes.func,
    getOrders: PropTypes.func,
    orders: PropTypes.array,
    meta: PropTypes.object,
    loaded: PropTypes.bool,
    children: PropTypes.element
  }

  @autobind
  closeModalHandler () {
    const { location } = this.props

    browserHistory.push({ ...location, query: { ...location.query, neworder: false } })
  }

  @autobind
  dateChangeHandler (date) {
    const { page, count } = this.props.location.query

    this.props.getOrders({
      page,
      count,
      start: moment(date).format('YYYY-MM-DD'),
      end: moment(date).format('YYYY-MM-DD')
    })
  }

  render () {
    const { location, children } = this.props
    const { query: { neworder } } = location
    const { orders, loaded } = this.props
    let Placeholder = (<div>Loading</div>)

    if (!loaded) {
      if (__CLIENT__) {
        const Spinner = require('react-spinkit')

        Placeholder = (
          <Row style={{ width: '100%', justifyContent: 'center' }}>
            <Spinner name="pacman" />
          </Row>
        )
      }
    }

    if (isEmpty(orders) && loaded) {
      Placeholder = (
        <Row style={{ width: '100%', justifyContent: 'center' }}>
          NO ORDERS
        </Row>
      )
    }

    return (
      <div>
        { neworder === 'true' &&
          <Window
            dismiss={this.closeModalHandler}
            style={{ width: 700, height: '75vh' }}
          >
            <AddOrders
              location={location}
              dismiss={this.closeModalHandler}
            />
          </Window>
        }
        <Row
          style={{
            justifyContent: 'space-between'
          }}
        >
          <h1>Orders</h1>
          <DateChanger
            unit="day"
            label="ORDERS FOR DAY"
            onChange={this.dateChangeHandler}
            location={location}
          />
          <Link
            to={{
              ...location,
              query: { ...location.query, neworder: true }
            }}
          >
            New order
          </Link>
        </Row>
        {(!loaded || isEmpty(orders)) ?
          Placeholder
        :
          React.cloneElement(children, {
            location
          })
        }
      </div>
    )
  }
}
