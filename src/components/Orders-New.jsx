import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { bindActionCreators } from 'redux'
import { asyncConnect } from 'redux-connect'
import { reduxForm, Field, formValueSelector } from 'redux-form'
import { groupBy } from 'lodash'
import moment from 'moment'
import { browserHistory } from 'react-router'
import { autobind } from 'core-decorators'
import Autocomplete from 'react-autocomplete'
import { postOrders, getOrders } from '_redux/modules/orders'
import { getMeals } from '_redux/modules/meals'
import { getMembers } from '_redux/modules/members'

import OrderField from './Orders-New-Field'
import Button from './common/Button'
import Spinner from 'components/common/Spinner'
import Title from './Title'
import { Column } from './Flexbox'
import { Radio } from './Fields'

import veganIcon from '../../static/images/icSpa.png'
import meatIcon from '../../static/images/icLocalDining.png'
import customIcon from '../../static/images/icBrightness.png'
import blueberries from '../../static/images/blueberries.jpg'

const selector = formValueSelector('order')

const getDate = () => {
  let date = moment().format('YYYY-MM-DD')

  if (moment(date).day() >= 3) {
    date = moment(date).add(1, 'week').day(1).format('YYYY-MM-DD')
  }

  return date
}

@asyncConnect([{
  promise: ({ store: { getState, dispatch }, location }) => {
    const date = location.query.date || getDate()

    const start = moment(date).startOf('week').format('YYYY-MM-DD')
    const end = moment(date).endOf('week').format('YYYY-MM-DD')

    if (!getState().meals.loaded) {
      return dispatch(getMeals({
        start,
        end
      }))
    }
  }
}],
  (state) => ({
    meals: state.meals.data,
    fullweek: selector(state, 'fullweek'),
    user: state.auth.user,
    members: state.members.data,
    membersLoaded: state.members.loaded,
    submitting: state.orders.submitting
  }),
  (dispatch) => bindActionCreators({
    postOrders,
    getMembers,
    getOrders,
    getMeals
  }, dispatch)
)
@reduxForm({
  form: 'order'
})
export default class NewOrders extends Component {
  static propTypes = {
    location: PropTypes.object,
    handleSubmit: PropTypes.func,
    meals: PropTypes.array,
    user: PropTypes.object,
    postOrders: PropTypes.func,
    getOrders: PropTypes.func,
    fullweek: PropTypes.string,
    dismiss: PropTypes.func,
    getMembers: PropTypes.func,
    membersLoaded: PropTypes.bool,
    members: PropTypes.array,
    member: PropTypes.string,
    submitting: PropTypes.bool
  }

  constructor (props) {
    super(props)
    this.state = {
      memberField: '',
      showCustom: false,
      submitting: false
    }
  }

  componentDidMount () {
    const { membersLoaded, user } = this.props

    if (__CLIENT__) {
      document.body.style.backgroundImage = `url(${blueberries})`
      document.body.style.backgroundColor = '#FECCCC'
      document.body.style.backgroundRepeat = 'no-repeat'
      document.body.style.backgroundSize = 'cover'
      document.body.style.backgroundAttachment = 'fixed'
    }

    if (!membersLoaded && user && user.role === 'ADMIN') {
      this.props.getMembers()
    }
  }

  componentWillUnmount () {
    if (__CLIENT__) {
      document.body.style.backgroundImage = undefined
      document.body.style.backgroundColor = undefined
      document.body.style.backgroundRepeat = undefined
      document.body.style.backgroundSize = undefined
      document.body.style.backgroundAttachment = undefined
    }
  }

  @autobind
  postOrdersHandler (data) {
    this.setState({ submitting: true })
    const { meals, dismiss, location, location: { query } } = this.props
    const _key = query.member || this.props.user._key
    const isDashboard = location.pathname.indexOf('dashboard') !== -1

    const orders = []

    const filterOnTheGo = (items) => (
      items.filter(meal => {
        const { tags } = meal
        const mealName = meal.name.toLowerCase()
        const isOnTheGo = (
          mealName.indexOf('on-the-go') > -1 || (tags && tags.includes('on-the-go'))
        )

        return !isOnTheGo
      })
    )

    const filterByDiet = (items, diet) => (
      items.filter(meal => {
        const isSameDiet = (meal.diet === diet)

        return isSameDiet
      })
    )

    const filterByDate = (items, date) => (
      items.filter(meal => {
        const mealDate = moment(meal.date).format('YYYY-MM-DD')
        const orderDate = moment(date).format('YYYY-MM-DD')
        const isSameDate = (mealDate === orderDate)

        return isSameDate
      })
    )

    /* Attemps to go through the object created by the form and get the order keys for everything selected. Keeping in mind the user can select 'full week' or 'full day' options. */
    for (const key in data) {
      if (key === 'fullweek') {
        /*
          If users picks "Full week [meal/vegan]", do this. Finds all non-"on-the-go" that match the diet.
          The data will look like `{ fullweek: 'vegan' }`
        */
        let matches = filterOnTheGo(meals)
        matches = filterByDiet(matches, data[key])

        const keys = matches.map(match => match._key)
        orders.push(...keys)
      } else if (typeof data[key] === 'object') {
        /*
          If user picks individual meals for a day.
          The data will look like `{ 2017-09-25: { breakfast: "655321", ... } }` where `655321` is the meal `_key`.
          There is also some useles data in this object like { 1: "c", 2: "u", ... } which has to be omitted.
        */
        const mealz = ['breakfast', 'lunch', 'dinner']

        for (let it = 0; it <= 2; it++) {
          if (data[key][mealz[it]]) {
            orders.push(data[key][mealz[it]])
          }
        }
      } else if (data[key] === 'meat' || data[key] === 'vegan') {
        /*
          This handles when a user selects "All day (vegan/regular)".
          `key` here is the date (e.g. `2017-09-25`)
          `data[key]` is a string, either `meat` or `vegan`
          `{ 2017-09-25: 'vegan' }`
        */
        let matches = filterByDiet(meals, data[key])
        matches = filterOnTheGo(matches)
        matches = filterByDate(matches, key)

        const keys = matches.map(match => match._key)
        orders.push(...keys)
      } else if (data[key] === true) {
        orders.push(key)
      }
    }

    this.props.postOrders({
      user: _key,
      orders
    })
    .then(() => {
      if (isDashboard) {
        this.props.getOrders({
          start: query.date,
          end: query.date
        })
      }

      this.setState({ submitting: false })

      if (dismiss) {
        return dismiss()
      }

      browserHistory.push('/order/done')
    })
  }

  @autobind
  memberChangeHandler (event) {
    this.setState({ memberField: event.target.value })
  }

  @autobind
  memberSelectHandler (value) {
    const { location, members } = this.props
    this.setState({ memberField: value })

    const member = members.filter(mem => mem.name === value)

    browserHistory.push({
      ...location,
      query: { ...location.query, member: member[0]._key }
    })
  }

  render () {
    const { memberField } = this.state
    const { meals, handleSubmit, fullweek, members, membersLoaded, location, location: { query } } = this.props
    const userIsAdmin = this.props.user && (this.props.user.role === 'ADMIN')
    const isDashboard = location.pathname.indexOf('dashboard') !== -1

    const days = groupBy(meals, (order) => moment(order.date).format('YYYY-MM-DD'))

    const renderDays = []

    for (const day in days) {
      renderDays.push(
        <OrderField
          key={renderDays.length}
          name={day}
          label={moment(day).format('dddd')}
          meals={days[day]}
        />
      )
    }

    return (
      <div
        style={{
          width: '100%',
          height: '100%'
        }}
      >
        <div
          style={{
            maxWidth: 670,
            margin: '0 auto',
            backgroundColor: 'white',
            height: '100%',
            minHeight: '100vh',
            padding: '0px 10px 100px 10px'
          }}
        >
          <Column
            style={{
              justifyContent: 'center',
              alignItems: 'center'
            }}
          >
            <Title />
            {membersLoaded && userIsAdmin && isDashboard &&
              <Column>
                <label>Select member</label>
                <Autocomplete
                  getItemValue={(item) => item.name}
                  items={members}
                  shouldItemRender={(item, value) =>
                    item.name.toLowerCase().indexOf(value.toLowerCase()) > -1
                  }
                  renderItem={(item, isHighlighted) =>
                    <div style={{ background: isHighlighted ? 'lightgray' : 'white' }}>
                      {item.name}
                    </div>
                  }
                  value={memberField}
                  onChange={this.memberChangeHandler}
                  onSelect={this.memberSelectHandler}
                />
              </Column>
            }
            <div
              style={{
                marginTop: 39,
                fontFamily: "Avenir Next Regular"
              }}
            >
              MEAL SELECTION FOR WEEK OF
            </div>
            <div
              style={{
                fontWeight: 700,
                fontSize: 21,
                fontFamily: 'Avenir Next Regular',
                marginBottom: 39
              }}
            >
              { query.date ?
                moment(query.date).format('MMMM DD')
              :
                moment(getDate()).format('MMMM DD')
              }
            </div>
          </Column>
          <form>
            <Field
              name="fullweek"
              component={Radio}
              onChange={this.fullWeekHandler}
              options={[
                { label: 'Full week vegan', value: 'vegan', icon: veganIcon },
                { label: 'Full week regular', value: 'meat', icon: meatIcon },
                { label: 'Custom', value: 'custom', icon: customIcon }
              ]}
            />
            { fullweek === 'custom' && renderDays }
            { (this.props.submitting || this.state.submitting) ?
              <Spinner style={{ marginTop: 33 }} />
            :
              <Column
                style={{
                  alignItems: 'center',
                  marginTop: 33
                }}
              >
                <Button
                  primary
                  disabled={this.props.submitting || this.state.submitting}
                  onClick={handleSubmit(this.postOrdersHandler)}
                >
                  Submit order
                </Button>
              </Column>
            }
          </form>
        </div>
      </div>
    )
  }
}
