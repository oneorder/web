import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { asyncConnect } from 'redux-connect'
import { Link } from 'react-router'
import Map, { Marker } from 'react-map-gl'
import dimensions from 'react-dimensions'

import { Row } from './Flexbox'
import { Controls } from './Addresses-Controls'

import { getAddresses } from '_redux/modules/addresses'

import classes from './Addresses.scss'
// const classes = {}

@asyncConnect([{
  promise: ({ store: { getState, dispatch }, location }) => {
    if (!getState().addresses.loaded) {
      return dispatch(getAddresses({
        count: location.query.count || 25,
        page: location.query.page || 1
      }))
    }
  }
}],
  (state) => ({
    addresses: state.addresses.data
  })
)
@dimensions()
export default class Addresses extends Component {
  static propTypes = {
    addresses: PropTypes.array,
    location: PropTypes.object,
    containerWidth: PropTypes.number
  }

  constructor (props) {
    super(props)
    this.state = {
      viewport: {
        latitude: 49.268241,
        longitude: -123.099823,
        zoom: 8
      }
    }
  }

  render () {
    const { addresses, containerWidth, location } = this.props
    const { zoom, longitude, latitude } = this.state.viewport
    let { page } = location.query
    const { hidemap } = location.query
    page = page && parseInt(page, 10)

    return (
      <div ref="container">
        <h1>Addresses</h1>
        <Controls location={location} />
        { __CLIENT__ && hidemap !== 'true' &&
          <Map
            style={{ marginBottom: 20 }}
            width={containerWidth}
            height={400}
            latitude={latitude}
            longitude={longitude}
            zoom={zoom}
            onViewportChange={(viewport) => {
              this.setState({ viewport })
            }}
          >
            {addresses && addresses.map((address, index) => (
              <Marker
                key={index}
                latitude={address.geo.latitude}
                longitude={address.geo.longitude}
                offsetLeft={-20}
                offsetTop={-10}
              >
                {zoom < 11 &&
                  <div className={classes.marker}>F</div>
                }
                {zoom >= 11 &&
                  <div>{address.street}</div>
                }
              </Marker>
            ))}
          </Map>
        }
        {addresses && addresses.map((address, index) => (
          <div
            className={classes.address}
            key={index}
          >
            <Row>
              <div>{address.street}</div>
            </Row>
          </div>
        ))}
        <Link
          to={{
            hostname: location.hostname,
            pathname: location.pathname,
            query: {
              ...location.query,
              page: page ? page + 1 : 2
            }
          }}
        >
          Next page
        </Link>
      </div>
    )
  }
}
