import React, { PropTypes, Component } from 'react'
import moment from 'moment'
import { browserHistory } from 'react-router'
import { autobind } from 'core-decorators'
import { Flex, Box } from 'grid-styled'

import Address from './Profile-Address'
import FormattedAddress from 'components/common/Address'
import Bar from 'components/common/Bar'

export default class MemberDetails extends Component {
  static propTypes = {
    member: PropTypes.object,
    location: PropTypes.object
  }

  static defaultProps = {
    member: {
      profile: {}
    }
  }

  constructor (props) {
    super(props)
    this.state = {
      editAddress: false
    }
  }

  @autobind
  editClickHanlder () {
    const { member, location } = this.props

    browserHistory.push({
      ...location,
      query: { ...location.query, member: member._key }
    })

    this.setState({ editAddress: true })
  }

  render () {
    const { location, member, member: { defaultAddress } } = this.props

    if (this.state.editAddress) {
      return (
        <Address
          onComplete={() => this.setState({ editAddress: false })}
          location={location}
        />
      )
    }

    return (
      <Flex wrap width={[1, 1, '830px']}>
        <Box
          p={1}
          width={[1, 1 / 2, 1 / 3]}
        >
          <Bar />
          <h2>Details</h2>
          <label>Name</label>
          <div>{member.name}</div>
          <label>Phone</label>
          <div>{member.phone}</div>
          <label>Joined</label>
          <div>
            {moment(member.createdAt).format('dddd MMMM DD, YYYY')}
          </div>
        </Box>
        <Box
          p={1}
          width={[1, 1 / 2, 1 / 3]}
        >
          <Bar />
          <h2>Profile</h2>
          <div>{member.profile.vegan ? 'Vegan' : 'Meat'}</div>
          {member.profile.allergies &&
            <div>
              <h3>Allergies</h3>
              {member.profile.allergies.map((allergy, index) => (
                <div key={index}>{allergy}</div>
              ))}
            </div>
          }
        </Box>
        {defaultAddress &&
          <Box
            p={1}
            width={[1, 1, 1 / 3]}
          >
            <Bar />
            <h2>Address (
              <span
                onClick={this.editClickHanlder}
                style={{
                  color: 'blue',
                  cursor: 'pointer'
                }}
              >
                Edit
              </span>
            )</h2>
            <FormattedAddress
              address={defaultAddress}
            />
          </Box>
        }
      </Flex>
    )
  }
}
