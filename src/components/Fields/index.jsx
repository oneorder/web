import React from 'react'
import PropTypes from 'prop-types'
import { capitalize } from 'lodash'
import { autobind } from 'core-decorators'
import moment from 'moment'

import classes from './index.scss'

import { Column } from '../Flexbox'
// import DayPicker from 'react-day-picker';
import DayPicker from 'react-day-picker';

import Radio from './Radio'
import TextFieldArray from './TextFieldArray'
import TextField from './TextField'
import Tags from './Tags'
import validate from './validate'

// const classes = {}

if (__CLIENT__) {
  require('react-day-picker/lib/style.css')
}

const Checkbox = ({ children, input, meta: { touched, error } }) => {
  const showError = touched && error;

  return (
    <div>
      <label
        className={input.value ? classes.labelChecked : classes.label}
        htmlFor={`Checkbox_${input.name}`}
      >
        <input
          {...input}
          id={`Checkbox_${input.name}`}
          className={input.value ? classes.checkboxChecked : classes.checkbox}
          type="checkbox"
        />
        <span className={classes.text}>{children}</span>
      </label>
      {showError &&
        <span className={classes.error}>{error}</span>
      }
    </div>
  );
};

class DatePicker extends React.Component {
  static propTypes ={
    input: PropTypes.object,
    label: PropTypes.string,
    meta: PropTypes.object
  }

  @autobind
  onChange (date) {
    const formatted = moment(date).format('YYYY-MM-DD')
    this.props.input.onChange(formatted)
  }

  render () {
    const { input, label, meta: { touched, error } } = this.props
    const showError = touched && error
    const id = `DatePicker_${input.name}`
    const dateFormat = 'ddd MMM DD YYYY HH:mm:ss [GMT]ZZ ([PDT])'
    const unFormatted = moment(input.value).add(12, 'hours').format(dateFormat)

    return (
      <div className={classes.textFieldContainer}>
        <label htmlFor={id}>{label}</label>
        <DayPicker
          {...input}
          onDayClick={this.onChange}
          id={id}
          format="dddd MMMM DD, YYYY"
          selectedDays={new Date(unFormatted)}
        />
        {showError &&
          <span className={classes.error}>{error}</span>
        }
      </div>
    )
  }
}

const Textarea = ({ label, input, meta: { touched, error } }) => {
  const showError = touched && error;
  const fieldId = `Textarea_${input.name}`

  return (
    <div className={classes.textFieldContainer}>
      <label
        className={classes.text}
        htmlFor={fieldId}
      >
        {label}
      </label>
      <textarea
        {...input}
        id={fieldId}
        className={classes.textInput}
      >
        {input.value}
      </textarea>
      {showError &&
        <span className={classes.error}>{error}</span>
      }
    </div>
  )
}

const Select = ({ style, options, label, input, error, touched, capitalize: caps }) => {
  const showError = touched && error

  return (
    <Column
      style={style}
    >
      <label htmlFor={input.name}>
        {label}
      </label>
      <select
        {...input}
        id={input.name}
      >
        {options.map((option, index) => {
          return (
            <option key={index} value={option}>
              {caps ? capitalize(option) : option}
            </option>
          )
        })}
      </select>
      {showError &&
        <span className={classes.error}>{error}</span>
      }
    </Column>
  )
}

const commonProps = {
  children: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.string,
  ]),
  input: PropTypes.object,
  meta: PropTypes.object,
  className: PropTypes.string
}

Checkbox.propTypes = commonProps
Select.propTypes = {
  ...commonProps,
  options: PropTypes.array
}

export {
  Checkbox,
  TextField,
  Textarea,
  TextFieldArray,
  Select,
  DatePicker,
  Radio,
  Tags,
  validate
}
