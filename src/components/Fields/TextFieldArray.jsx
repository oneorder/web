import React from 'react'
import PropTypes from 'prop-types'
import { Field } from 'redux-form'

import { TextField } from './index'
import Button from 'components/common/Button'
import { Row } from '../Flexbox'

export const validateRequired = (value) => value ? undefined : "Required field."

const TextFieldArray = ({ fields, buttonLabel, label }) => {
  return (
    <div>
      {label &&
        <label htmlFor={fields.name}>
          {label}
        </label>
      }
      {fields.map((field, index) => {
        return (
          <Row style={{ width: '100%' }}>
            <Field
              name={field}
              key={index}
              component={TextField}
              style={{ flexGrow: 1 }}
            />
            <Button
              noSubmit
              onClick={() => fields.remove(index)}
              style={{
                height: 39,
                borderLeft: 'none'
              }}
            >
              X
            </Button>
          </Row>
        )
      })}
      <Button
        onClick={() => fields.push()}
        noSubmit
        small
      >
        {buttonLabel}
      </Button>
    </div>
  )
}

TextFieldArray.propTypes = {
  label: PropTypes.string,
  children: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.string,
  ]),
  input: PropTypes.object,
  meta: PropTypes.object,
  className: PropTypes.string
}

export default TextFieldArray
