import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Field } from 'redux-form'
import { connect } from 'react-redux'

import {
  TextField,
  validate
} from './Fields'
import { Row } from './Flexbox'

@connect(
  (state) => ({
    user: state.auth.user
  })
)
export default class Register extends Component {
  static propTypes = {
    register: PropTypes.func,
    user: PropTypes.object
  }

  render () {
    return (
      <div>
        <Row>
          <Field
            name="address.street"
            component={TextField}
            validate={validate.isRequired}
            label="Street"
            style={{ width: '67%' }}
          />
          <Field
            name="address.unit"
            component={TextField}
            label="Unit"
            style={{ width: '33%' }}
          />
        </Row>
        <Row>
           <Field
            name="address.city"
            component={TextField}
            validate={validate.isRequired}
            label="City"
            style={{ width: '67%' }}
          />
          <Field
            name="address.postal"
            component={TextField}
            validate={validate.isPostalCode}
            label="Postal"
            style={{ width: '33%' }}
          />
        </Row>
        <Field
          name="address.instructions"
          component={TextField}
          label="Delivery instructions"
          placeholder="e.g. Call, ring buzzer 123, etc"
        />
        {this.props.user && this.props.user.role === 'ADMIN' &&
          <Field
            name="address.notes"
            component={TextField}
            placeholder="Notes for internal use"
            label="Notes"
          />
        }
      </div>
    )
  }
}
