import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Link, browserHistory } from 'react-router'
import { autobind } from 'core-decorators'
import moment from 'moment'
import { isEmpty } from 'lodash'

import { Column } from './Flexbox'
import Window from './Window'

const AllergiesModal = ({allergies}) => (
  <div>
    {allergies.map((user, index) => (
      <div
        key={index}
        style={{
          padding: '9px 16px',
          border: '1px solid black'
        }}
      >
        <label>Member</label>
        <div>{user.name}</div>
        <label>Allergies</label>
        {user.allergies.map((allergy, ind) => (<div key={ind}>{allergy}</div>))}
      </div>
    ))}
  </div>
)

export default class Meals extends Component {
  static propTypes = {
    location: PropTypes.object,
    meal: PropTypes.object
  }

  constructor (props) {
    super(props)
    this.state = {
      date: moment().format('YYYY-MM-DD')
    }
  }

  @autobind
  closeModalHandler () {
    const { location } = this.props

    browserHistory.push({ ...location, query: { ...location.query, allergies: undefined } })
  }

  render () {
    const { location, meal: { meal, count, allergies } } = this.props
    const { query } = location

    return (
      <div>
        <Column
          style={{
            height: 150,
            width: 150,
            backgroundColor: 'black',
            color: 'white',
            borderRadius: '50%',
            alignItems: 'center',
            justifyContent: 'center',
            marginRight: 10,
            marginBottom: 10
          }}
        >
          <div style={{ fontSize: 11, marginBottom: 5, fontWeight: 700 }}>
            {`${meal.diet.toUpperCase()} ${meal.meal.toUpperCase()}`}
          </div>
          <div style={{ textAlign: 'center' }}>
            {meal.name}
          </div>
          <div style={{ fontSize: 25, fontWeight: 500 }}>{count}</div>
          { !isEmpty(allergies) &&
            <Link
              style={{
                color: 'white',
                textDecoration: 'underline'
              }}
              to={{
                ...location,
                query: { ...location.query, allergies: meal._key }
              }}
            >
              {allergies.length} allergies
            </Link>
          }
        </Column>
        {query.allergies === meal._key &&
          <Window
            dismiss={this.closeModalHandler}
          >
            <AllergiesModal
              allergies={allergies}
            />
          </Window>
        }
      </div>
    )
  }
}
