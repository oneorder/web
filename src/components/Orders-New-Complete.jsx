import React, { Component } from 'react'
import PropTypes from 'prop-types'

import Title from './Title'
import { Column } from './Flexbox'

export default class Meals extends Component {
  static propTypes = {
    location: PropTypes.object
  }

  render () {
    return (
      <Column
        style={{
          alignItems: 'center',
          width: '100%',
          marginTop: 10
        }}
      >
        <Column
          style={{
            width: 670,
            maxWidth: 670,
            backgroundColor: 'white',
            paddingBottom: 15
          }}
        >
          <Column
            style={{
              justifyContent: 'center',
              alignItems: 'center'
            }}
          >
            <Title />
            <h2>Thanks for ordering!</h2>
          </Column>
        </Column>
      </Column>
    )
  }
}
