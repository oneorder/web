import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { asyncConnect } from 'redux-connect'
import moment from 'moment'
import { Flex } from 'grid-styled'

import Item from './Orders-List-Item'
import Export from './Orders-Export'

@asyncConnect([],
  (state) => ({
    orders: state.orders.data,
    meta: state.orders.meta,
    loaded: state.orders.loaded
  })
)
export default class Meals extends Component {
  static propTypes = {
    location: PropTypes.object,
    meals: PropTypes.array,
    orders: PropTypes.array,
    meta: PropTypes.object,
    loaded: PropTypes.bool
  }

  constructor (props) {
    super(props)
    this.state = {
      date: moment().format('YYYY-MM-DD')
    }
  }

  render () {
    const { orders, loaded, location } = this.props

    return (
      <div>
        <Flex justify="flex-end">
          <Export />
        </Flex>
        <Flex wrap>
          {loaded && orders.map((order, index) => {
            return <Item location={location} order={order} key={index} />
          })}
        </Flex>
      </div>
    )
  }
}
