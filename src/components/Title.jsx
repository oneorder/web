import React from 'react'

import classes from './AppLayout.scss'
// const classes = {}

const Title = () => (
  <h1 className={classes.title}>ONEORDER</h1>
)

export default Title
