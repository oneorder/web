import React from 'react'
import { Link } from 'react-router'

import { Row } from './Flexbox'

export const Controls = ({ location, location: {query: {hidemap} } }) => {
  return (
  <Row
    style={{
      justifyContent: 'space-between'
    }}
  >
    <div>
      <span>{`Entries: `}</span>
      {[25, 50, 100].map((count, index) => (
        <span>
          <Link
            key={index}
            to={{
              hostname: location.hostname,
              pathname: location.pathname,
              query: {
                ...location.query,
                count
              }
            }}
          >
            {count}
          </Link>
          {index !== 2 &&
            <span>, </span>
          }
        </span>
      ))}
    </div>
    <div>
      <Link
        to={{
          hostname: location.hostname,
          pathname: location.pathname,
          query: {
            ...location.query,
            hidemap: hidemap === 'false' ? true : false
          }
        }}
      >
        Toggle map
      </Link>
    </div>
  </Row>
  )
}
