import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router'

import submitIcon from '../../../static/images/icGrain.png'
import classes from 'components/common/Button.scss'
// const classes = {}

const Button = (props) => {
  const {
    children, noSubmit, small, onClick, to, icon, style, primary, disabled
  } = props
  const className = !small ? classes.button : classes.buttonSmall

  const containerStyle = {
    display: 'flex',
    winWidth: 221,
    marginBottom: 5,
    ...primary ? {
      backgroundColor: '#333',
      color: '#fff',
      width: '221px'
    } : {},
    ...disabled ? {
      opacity: 0.5
    } : {},
    ...style
  }

  const textStyle = {
    flexGrow: 1,
    justifyContent: 'center',
    alignItems: 'center',
    display: 'flex'
  }

  if (to) {
    return (
      <Link
        to={to}
        className={className}
        style={containerStyle}
      >
        {(icon || primary) &&
          <div>
            <img src={icon || primary} />
          </div>
        }
        {children}
      </Link>
    )
  }

  if (noSubmit) {
    return (
      <input
        className={className}
        type="button"
        value={children}
        onClick={onClick}
        style={containerStyle}
        disabled={disabled}
      />
    )
  }

  return (
    <button
      className={!small ? classes.btn : classes.btnSmall}
      style={containerStyle}
      onClick={onClick}
      disabled={disabled}
    >
      {(icon || primary) &&
        <div>
          <img src={icon || submitIcon} />
        </div>
      }
      <span style={textStyle}>
        {children}
      </span>
    </button>
  )
}

Button.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.string
  ]),
  noSubmit: PropTypes.func,
  onClick: PropTypes.func,
  small: PropTypes.bool,
  to: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.object
  ]),
  icon: PropTypes.string,
  style: PropTypes.object,
  primary: PropTypes.bool,
  disabled: PropTypes.bool
}

export default Button
