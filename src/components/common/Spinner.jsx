import React from 'react'
import PropTypes from 'prop-types'
import { Row } from './Flexbox'

const Spinner = ({ name, style }) => {
  if (__CLIENT__) {
    const SpinKit = require('react-spinkit')
    return (
      <Row style={{
        width: '100%',
        justifyContent: 'center',
        ...style
      }}>
        <SpinKit name={ name || "pacman"} />
      </Row>
    )
  }

  return (
    <Row style={{
      width: '100%',
      justifyContent: 'center',
      ...style
    }}>
      Loading...
    </Row>
  )
}

Spinner.propTypes = {
  name: PropTypes.string,
  style: PropTypes.object
}

export default Spinner
