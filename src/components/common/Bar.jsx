import React from 'react'

import blueberries from '../../../static/images/blueberries.jpg'
import limes from '../../../static/images/limes.jpg'
import beach from '../../../static/images/beach.jpg'

const Bar = ({ style, black }) => {
  const num = Math.floor(Math.random() * 10) % 3
  let backgroundImage = num === 0 ? blueberries : limes

  if (num === 1) {
    backgroundImage = beach
  }

  return (
    <div
      style={{
        height: 21,
        width: '100%',
        backgroundImage: black ? null : `url(${backgroundImage})`,
        backgroundColor: '#333',
        backgroundPosition: 'center',
        backgroundSize: 400,
        ...style
      }}
    />
  )
}

export default Bar
