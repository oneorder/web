import React from 'react'

import { Column } from './Flexbox'

const Address = ({ style, address: { unit, street, city, postal, state, country, instructions, notes }}) => {
  return (
    <Column
      style={{
        fontFamily: 'Courier New, Courier New, monospace',
        fontWeight: 'bold',
        fontSize: 17,
        ...style
      }}
    >
      <div>
        {unit &&
          <span title="Unit">{unit.toUpperCase() + ' '}</span>
        }
        {street &&
          <span title="Street">{street.toUpperCase()}</span>
        }
      </div>
      <div>
        {city &&
          <span title="City">{city.toUpperCase() + ' '}</span>
        }
        {state &&
          <span title="State">{state.toUpperCase()}</span>
        }
        {country &&
          <span title="Country">{', ' + country.toUpperCase()}</span>
        }
      </div>
      <div
        style={{ marginBottom: (instructions || notes) ? 10 : 0 }}
      >
        {postal &&
          <span title="Postal">{postal.toUpperCase()}</span>
        }
      </div>
      { (instructions || notes) &&
        <Column>
          {instructions &&
            <span title="Instructions">
              INST: {instructions.toUpperCase()}
            </span>
          }
          {notes &&
            <span title="Notes">
              NOTES: {notes.toUpperCase()}
            </span>
          }
        </Column>
      }
    </Column>
  )
}

export default Address
