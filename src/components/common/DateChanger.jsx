import React, { Component } from 'react'
import PropTypes from 'prop-types'
import moment from 'moment'
import { browserHistory } from 'react-router'

import { Row, Column } from './Flexbox'

export default class DateChanger extends Component {
  static propTypes = {
    location: PropTypes.object,
    initialValue: PropTypes.string,
    onChange: PropTypes.func,
    unit: PropTypes.string,
    label: PropTypes.string
  }

  static defaultProps = {
    initialValue: moment().format(),
    unit: 'day'
  }

  componentDidUpdate (oldProps) {
    const { location: { query: { date } } } = this.props

    if (date !== oldProps.location.query.date) {
      this.props.onChange(date)
    }
  }

  dateChangeHandler (direction) {
    const { location: { query: { date } }, initialValue, unit } = this.props
    const currentDate = date || initialValue
    let newDate

    if (direction === 'back') {
      newDate = moment(currentDate).subtract(1, unit).format('YYYY-MM-DD')
    } else if (direction === 'forward') {
      newDate = moment(currentDate).add(1, unit).format('YYYY-MM-DD')
    }

    browserHistory.push({
      ...location,
      query: { ...location.query, date: newDate }
    })
  }

  render () {
    const { label, initialValue, location: { query: { date }} } = this.props

    return (
      <Column>
        {label &&
          <div style={{ textAlign: 'center' }}>
            {label}
          </div>
        }
        <Row>
          <div
            style={{ cursor: 'pointer' }}
            onClick={() => this.dateChangeHandler('back')}
          >
            {'<-'}
          </div>
          <div>{moment(date || initialValue).format('dddd MMMM DD, YYYY')}</div>
          <div
            style={{ cursor: 'pointer' }}
            onClick={() => this.dateChangeHandler('forward')}
          >
            {'->'}
          </div>
        </Row>
      </Column>
    )
  }
}
