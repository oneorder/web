import React from 'react'
import PropTypes from 'prop-types'

const Row = ({ children, className, style }) => {
  return (
    <div
      style={{
        display: 'flex',
        flexDirection: 'row',
        ...style
      }}
      className={className}
    >
      {children}
    </div>
  )
}

const Column = ({ onClick, children, className, style }) => {
  return (
    <div
      style={{
        display: 'flex',
        flexDirection: 'column',
        ...style
      }}
      className={className}
      onClick={onClick}
    >
      {children}
    </div>
  )
}

const Container = ({ style, children, margin }) => {
  return (
    <div
      style={{
        paddingBottom: margin || 3,
        paddingRight: margin || 3,
        ...style
      }}
    >
      { children }
    </div>
  )
}

const commonProps = {
  children: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.string,
    PropTypes.array
  ]),
  className: PropTypes.string
}

Row.propTypes = commonProps
Column.propTypes = commonProps
Container.propTypes = {
  ...commonProps,
  margin: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.string
  ])
}

export { Row, Column, Container }
