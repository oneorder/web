import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { browserHistory } from 'react-router'
import { autobind } from 'core-decorators'
import { getMe, logout } from '_redux/modules/auth'
import { postMember } from '_redux/modules/members'
import Alert from 'react-s-alert'

import Title from './Title'
import Window from './Window'
import Button from 'components/common/Button'
import Login from './Login'
import { Column, Row } from 'components/common/Flexbox'

import classes from './AppLayout.scss'
// const classes = {}

if (__CLIENT__) {
  require('react-s-alert/dist/s-alert-default.css');
  require('react-s-alert/dist/s-alert-css-effects/slide.css');
}

@connect(
  (state) => ({
    user: state.auth.user
  }),
  (dispatch) => bindActionCreators({
    getMe,
    postMember,
    logout
  }, dispatch)
)
export default class Home extends Component {
  static propTypes = {
    children: PropTypes.element,
    location: PropTypes.object,
    getMe: PropTypes.func,
    postMember: PropTypes.func,
    logout: PropTypes.func,
    user: PropTypes.object
  }

  componentDidMount () {
    const { user } = this.props

    if (!user) {
      return this.props.getMe()
      .catch((error) => {
        if (error) {
          this.showLoginModal()
        }
      })
    }
  }

  showLoginModal () {
    const { location } = this.props

    browserHistory.push({
      ...location,
      query: { ...location.query, login: true }
    })
  }

  @autobind
  logoutHandler () {
    this.props.logout()
    .then(() => {
      this.showLoginModal()
    })
  }

  render () {
    const { user, children, location, location: { query: { register, login } } } = this.props
    let containerClass = classes.loginContainer
    const isLogin = location.pathname === '/'

    if (register) {
      containerClass = classes.registerContainer
    }

    const dismiss = () => {
      if (this.props.user) {
        browserHistory.push({
          ...location,
          query: {
            ...location.query,
            register: undefined,
            login: undefined
          }
        })
      }
    }

    return (
      <div>
        {(login || register) &&
          <Window
            location={location}
            dismiss={dismiss}
            style={{
              width: 500
            }}
            hideBack
          >
            <Login
              dismiss={dismiss}
              registering={register}
              location={location}
              hideLogin
            />
          </Window>
        }
        {location.pathname === '/' &&
          <Row style={{ justifyContent: 'center' }}>
            <Column style={{ flexGrow: 1, maxWidth: 221 }}>
              <Column
                style={{
                  margin: '10px 0',
                  paddingBottom: 5,
                  alignItems: 'center',
                  backgroundColor: 'white'
                }}
              >
                <Title />
              </Column>
              <Button to="/order">
                Order
              </Button>
              <Button to="/profile">
                Edit profile
              </Button>
              {user && user.role === 'ADMIN' &&
                <Button to="/dashboard">
                  Admin dashboard
                </Button>
              }
              <div
                onClick={this.logoutHandler}
                style={{
                  cursor: 'pointer',
                  textAlign: 'center',
                  backgroundColor: 'white',
                  padding: '5px 0',
                  border: '1px solid black'
                }}
              >
                Logout
              </div>
            </Column>
          </Row>
        }
        {children &&
          <div className={isLogin && containerClass}>
            {React.cloneElement(children, {
              location,
              registering: (register)
            })}
          </div>
        }
        <Alert stack={{limit: 3}} />
      </div>
    )
  }
}
