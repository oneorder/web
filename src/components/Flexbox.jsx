import React from 'react'
import PropTypes from 'prop-types'

const Row = ({ children, className, style }) => {
  return (
    <div
      style={{
        display: 'flex',
        flexDirection: 'row',
        ...style
      }}
      className={className}
    >
      {children}
    </div>
  )
}

const Column = ({ onClick, children, className, style }) => {
  return (
    <div
      style={{
        display: 'flex',
        flexDirection: 'column',
        ...style
      }}
      className={className}
      onClick={onClick}
    >
      {children}
    </div>
  )
}

const commonProps = {
  children: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.string,
    PropTypes.array
  ]),
  className: PropTypes.string
}

Row.propTypes = commonProps
Column.propTypes = commonProps

export { Row, Column }
