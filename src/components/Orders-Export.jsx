import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { autobind } from 'core-decorators'
import json2csv from 'json2csv'

import Button from 'components/common/Button'

import routificLogo from '../../static/images/routific.png'

@connect(
  (state) => ({
    orders: state.orders.data
  })
)
export default class Export extends Component {
  static propTypes = {
    orders: PropTypes.array
  }

  constructor (props) {
    super(props)
    this.state = {
      csvData: false
    }
  }

  @autobind
  csvExportHandler () {
    const fields = ['ID', 'Address', 'City', 'State', 'Zip code', 'From', 'To', 'Duration', 'Load', 'Phone', 'Email', 'Types', 'Priority', 'Notes', 'Notes 2']

    const orders = this.props.orders.map(({ user }) => ({
      ID: user.name,
      Address: `${user.address.street} ${user.address.city}, BC ${user.address.postal}`,
      City: user.address.city,
      State: 'BC',
      'Zip code': user.address.postal,
      Phone: user.phone,
      Email: user.email,
      Notes: `${user.address.unit && 'Unit/Apt: ' + user.address.unit + '. '}${user.address.notes}`,
      'Notes 2': user.address.instructions
    }))

    const csv = json2csv({ data: orders, fields })
    const base64csv = btoa(csv)

    this.setState({ csvData: `data:text/csv;charset=utf-8;base64,${base64csv}` })
  }

  render () {
    if (this.state.csvData) {
      return (
        <a
          href={this.state.csvData}
          download="oneorderData.csv"
          style={{
            height: '51px',
            display: 'flex',
            alignItems: 'center',
            width: '162px',
            justifyContent: 'space-around'
          }}
        >
          <i className="fa fa-download" aria-hidden="true" /> Download ready
        </a>
      )
    }

    return (
      <Button
        onClick={this.csvExportHandler}
        icon={routificLogo || undefined}
      >
        Get Routific CSV
      </Button>
    )
  }
}
