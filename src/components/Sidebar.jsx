import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { browserHistory } from 'react-router'
import { autobind } from 'core-decorators'
import { asyncConnect } from 'redux-connect'
import { bindActionCreators } from 'redux'
import { Flex } from 'grid-styled'
import { postMember } from '_redux/modules/members'

import Button from 'components/common/Button'
import Title from './Title'

import classes from './Sidebar.scss'
// const classes = {}

import blueberries from '../../static/images/blueberries.jpg'

@asyncConnect([],
  () => ({}),
  (dispatch) => bindActionCreators({
    postMember
  }, dispatch)
)
export default class Sidebar extends Component {
  static propTypes = {
    children: PropTypes.element,
    location: PropTypes.object,
    projects: PropTypes.array,
    postMember: PropTypes.func
  }

  @autobind
  logout () {
    if (__CLIENT__) {
      delete window.localStorage.authToken
      browserHistory.push('/')
    }
  }

  render () {
    const {
      children,
      location: { query }
    } = this.props

    return (
      <div
        style={{
          backgroundImage: `url(${blueberries})`,
          backgroundAttachment: 'fixed'
        }}
      >
        <div className={classes.sidebarContainer}>
          <div className={classes.title}>
            <Title />
          </div>
          <Button to="/dashboard">Home</Button>
          <Button to="/dashboard/addresses">Addresses</Button>
          <Button to="/dashboard/members">Members</Button>
          <Button to="/dashboard/meals">Meals</Button>
          <Flex
            py={1}
            my={1}
            align="center"
            justify="center"
            style={{
              backgroundColor: '#333',
              color: 'white'
            }}
          >
            Orders
          </Flex>
          <Button
            to={{
              pathname: "/dashboard/deliveries",
              query: { date: query.date || undefined }
            }}
          >
            Deliveries
          </Button>
          <Button
            to={{
              pathname: "/dashboard/kitchen",
              query: { date: query.date || undefined }
            }}
          >
            Kitchen
          </Button>
          <Button onClick={this.logout}>Log out</Button>
        </div>
        <div className={classes.contentContainer}>
          {children && React.cloneElement(children)}
        </div>
      </div>
    )
  }
}
