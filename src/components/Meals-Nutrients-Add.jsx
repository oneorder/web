import React from 'react'
import { capitalize } from 'lodash'

const Add = ({ nutrient, push }) => (
  <div
    onClick={() => push(nutrient)}
    style={{
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      textAlign: 'center',
      height: 50,
      width: 50,
      padding: 0,
      marginBottom: 15,
      marginRight: 15,
      border: 'solid 1px black',
      backgroundColor: nutrient.color,
      borderRadius: '50%',
      color: nutrient.color === 'white' ? 'black' : 'white',
      wordBreak: 'break-all',
      hyphens: 'auto'
    }}
  >
    {capitalize(nutrient.name)}
  </div>
)

export default Add
