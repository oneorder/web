import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { asyncConnect } from 'redux-connect'
import { isEmpty, groupBy, capitalize, sortBy } from 'lodash'

import { Row, Column } from './Flexbox'
import Hero from './Orders-Hero'

@asyncConnect([],
  (state) => ({
    orders: state.orders.data,
    meta: state.orders.meta,
    loaded: state.orders.loaded
  })
)
export default class Orders extends Component {
  static propTypes = {
    location: PropTypes.object,
    handleSubmit: PropTypes.func,
    meals: PropTypes.array,
    getOrders: PropTypes.func,
    orders: PropTypes.array,
    meta: PropTypes.object,
    loaded: PropTypes.bool
  }

  static defaultProps = {
    meta: {
      counts: []
    }
  }

  render () {
    const { location, meta, loaded } = this.props

    const meals = loaded && groupBy(meta.counts, (obj) => obj.meal.meal)

    const renderMeal = (mealName) => {
      const sortedMeals = sortBy(meals[mealName], 'diet')

      if (isEmpty(sortedMeals)) {
        return <span></span>
      }

      return (
        <div>
          <label>{capitalize(mealName)}</label>
          <Row style={{ flexWrap: 'wrap' }}>
            {meals[mealName] && sortedMeals.map((meal, index) => {
              return (
                <Hero
                  key={index}
                  location={location}
                  meal={meal}
                />
              )
            })}
          </Row>
        </div>
      )
    }

    return (
      <Column>
        { renderMeal('breakfast') }
        { renderMeal('lunch') }
        { renderMeal('dinner') }
      </Column>
    )
  }
}
