import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { reduxForm, SubmissionError } from 'redux-form'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { autobind } from 'core-decorators'
import { patchMember } from '_redux/modules/members'
import { getMe } from '_redux/modules/auth'

import Register from './Register'
import Button from 'components/common/Button'
import { Column } from 'components/common/Flexbox'

import submitIcon from '../../static/images/icGrain.png'

const invalidFields = {
  defaultAddress: undefined,
  _id: undefined,
  _key: undefined,
  role: undefined
}

@connect(
  (state) => ({
    user: state.auth.user,
    initialProps: {
      ...state.auth.user,
      ...invalidFields
    }
  }),
  (dispatch) => bindActionCreators({
    patchMember,
    getMe
  }, dispatch)
)
@reduxForm({
  form: 'profile'
})
export default class Profile extends Component {
  static propTypes = {
    location: PropTypes.object,
    handleSubmit: PropTypes.func,
    patchMember: PropTypes.func,
    getMe: PropTypes.func,
    user: PropTypes.object,
    initialize: PropTypes.func,
    logout: PropTypes.func,
    submitting: PropTypes.bool,
    pristine: PropTypes.bool
  }

  componentDidMount () {
    this.props.initialize({
      ...this.props.user,
      ...invalidFields
    })
  }

  componentDidUpdate (oldProps) {
    if (oldProps.user !== this.props.user) {
      this.props.initialize({
        ...this.props.user,
        ...invalidFields
      })
    }
  }

  @autobind
  patchMemberHandler (fields) {
    const { user, location: { query } } = this.props

    if (query.member && user.role === 'ADMIN') {
      return this.props.patchMember({
        _key: query.member,
        data: fields
      })
    }

    return this.props.patchMember({
      _key: user._key,
      data: fields
    })
    .then(() => {
      this.props.getMe()
    })
    .catch((error) => {
      console.error(error)
      throw new SubmissionError({ error: error })
    })
  }

  render () {
    const { handleSubmit, submitting, pristine } = this.props

    return (
      <form
        style={{
          width: '100%'
        }}
        onSubmit={handleSubmit(this.patchMemberHandler)}
      >
        <h1>Update profile</h1>
        <Register hidePassword hideAddress />
        <Column
          style={{
            alignItems: 'center',
            marginTop: 30
          }}
        >
          <Button
            disabled={pristine || submitting}
            icon={submitIcon}
            style={{
              backgroundColor: '#333333',
              color: '#fff',
              width: '221px'
            }}
          >
            Submit changes
          </Button>
        </Column>
      </form>
    )
  }
}
