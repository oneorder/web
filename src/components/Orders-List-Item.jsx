import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { bindActionCreators } from 'redux'
import { asyncConnect } from 'redux-connect'
import moment from 'moment'
import { capitalize, truncate, groupBy } from 'lodash'
import { Box } from 'grid-styled'
import { getOrders, deleteOrder } from '_redux/modules/orders'

import { Row, Column } from 'components/common/Flexbox'
import Address from 'components/common/Address'
import Bar from 'components/common/Bar'

import breakfast from '../../static/images/breakfast@2x.png'
import lunch from '../../static/images/lunch@2x.png'
import dinner from '../../static/images/dinner@2x.png'

import meatIcon from '../../static/images/pigIcon.svg'

@asyncConnect([],
  () => ({}),
  (dispatch) => bindActionCreators({
    getOrders,
    deleteOrder
  }, dispatch)
)
export default class Meals extends Component {
  static propTypes = {
    location: PropTypes.object,
    getOrders: PropTypes.func,
    deleteOrder: PropTypes.func,
    order: PropTypes.object
  }

  deleteOrderHandler (order) {
    const { page, count, date } = this.props.location.query
    const confirmDelete = confirm(`Are you sure you'd like to delete ${moment(order.date).format('dddd')}'s order of ${order.name}?`)

    if (confirmDelete) {
      this.props.deleteOrder(order._key)
      .then(() => {
        this.props.getOrders({
          page,
          count,
          start: moment(date).format('YYYY-MM-DD'),
          end: moment(date).format('YYYY-MM-DD')
        })
      })
    }
  }

  render () {
    const { order } = this.props

    const mealGroups = groupBy(order.meals, ({meal}) => meal)
    const diets = groupBy(order.meals, ({diet}) => diet)
    const meals = [
      ...mealGroups.dinner || [],
      ...mealGroups.lunch || [],
      ...mealGroups.breakfast || []
    ]

    return (
      <Box
        width={[1, 1, 1, 1 / 2]}
        p={1}
      >
        <Bar black />
        <Row
          style={{
            justifyContent: 'flex-end',
            alignItems: 'center',
            marginTop: 10,
          }}
        >
          <h2 style={{
            flexGrow: 1,
            margin: 0,
            fontFamily: 'Futura Condensed Bold'
          }}>
            {order.user.name}
          </h2>
          {diets.meat &&
            <div>
              <img
                style={{ height: 43, width: 'auto' }}
                src={meatIcon}
                alt="Ordered regular dishes"
                title="Ordered regular dishes"
              />
              <span style={{ display: 'inline-block', marginTop: 13}}>x {diets.meat.length}</span>
            </div>
          }
          {diets.vegan &&
            <div style={{ marginLeft: 5 }}>
              <span
                style={{
                  height: 25,
                  width: 25,
                  display: 'inline-flex',
                  backgroundColor: '#333',
                  color: 'white',
                  justifyContent: 'center',
                  alignItems: 'center',
                  borderRadius: '50%',
                  marginRight: 3
                }}
                src={meatIcon}
                alt="Ordered vegan dishes"
                title="Ordered vegan dishes"
              >
                V
              </span>
              <span>x {diets.vegan.length}</span>
            </div>
          }
        </Row>
        <Row>
          <Column style={{ width: '50%' }}>
            <h3>Address</h3>
            <Address
              address={order.user.address}
            />
          </Column>
          <Column style={{ width: '50%' }}>
            <h3>Orders ({meals.length})</h3>
            { meals.map((meal, ind) => {
              let image = meal.meal === 'dinner' ? dinner : breakfast

              if (meal.meal === 'lunch') {
                image = lunch
              }

              const backgroundColor = (ind % 2 === 0) ? '#eee' : 'transparent'

              return (
                <Row
                  key={ind}
                  style={{
                    backgroundColor,
                    padding: '0 5px',
                    alignItems: 'center'
                  }}
                >
                  <span
                    style={{
                      width: 41,
                      display: 'inline-block'
                    }}
                  >
                    {capitalize(meal.diet)}
                  </span>
                  <img
                    src={image}
                    style={{
                      height: 15,
                      width: 15,
                      margin: '0 5px'
                    }}
                    alt={meal.meal}
                    title={meal.meal}
                  />
                  <span
                    style={{
                      flexGrow: 1
                    }}
                  >
                    {truncate(meal.name, { length: 20 })}
                  </span>
                  <div
                    style={{
                      cursor: 'pointer',
                      color: 'red',
                      marginLeft: 10
                    }}
                    onClick={() => this.deleteOrderHandler(meal)}
                  >
                    X
                  </div>
                </Row>
              )
            })}
            {order.user.allergies &&
              <div>
                <h3>Allergies</h3>
                {order.user.allergies.map((al, ind) => <div key={ind}>{al}</div>)}
              </div>
            }
          </Column>
        </Row>
      </Box>
    )
  }
}
