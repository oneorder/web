import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Field, FieldArray } from 'redux-form'

import {
  Radio,
  TextField,
  TextFieldArray,
  validate
} from './Fields'
import Address from './Register-Address'

import classes from './Register.scss'

import veganIcon from '../../static/images/icSpa.png'
import meatIcon from '../../static/images/icLocalDining.png'

// const classes = {}

export default class Register extends Component {
  static propTypes = {
    builder: PropTypes.bool,
    hideAddress: PropTypes.bool,
    hidePassword: PropTypes.bool,
    children: PropTypes.element,
    location: PropTypes.object,
    register: PropTypes.func
  }

  render () {
    const { hidePassword, hideAddress } = this.props

    return (
      <div
        className={classes.container}
      >
        { !hideAddress &&
          <h1>Profile</h1>
        }
        <div>
          <Field
            label="Diet"
            name="profile.diet"
            component={Radio}
            options={[
              { label: 'Vegan', value: 'VEGAN', icon: veganIcon },
              { label: 'Regular', value: 'REGULAR', icon: meatIcon }
            ]}
          />
        </div>
        <FieldArray
          name="profile.allergies"
          label="Allergies"
          buttonLabel="Add allergy"
          component={TextFieldArray}
        />
        <Field
          name="name"
          label="Name"
          component={TextField}
          validate={validate.isRequired}
          type="text"
        />
        <Field
          name="phone"
          type="phone"
          component={TextField}
          validate={[
            validate.isRequired,
            validate.isMobilePhone
          ]}
          label="Number"
        />
        <Field
          name="email"
          component={TextField}
          validate={[
            validate.isRequired,
            validate.isEmail
          ]}
          label="Email"
        />
        {!hidePassword &&
          <Field
            name="password"
            type="password"
            component={TextField}
            validate={validate.isRequired}
            label="Password"
          />
        }
        <Field
          name="dateOfBirth"
          component={TextField}
          type="date"
          label="Date of birth"
          placeholder="YYYY/MM/DD"
          validate={validate.isBefore}
        />
        {!this.props.hideAddress &&
          <Address />
        }
      </div>
    )
  }
}
