import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Field, reduxForm, FieldArray, SubmissionError } from 'redux-form'
import { bindActionCreators } from 'redux'
import { asyncConnect } from 'redux-connect'
import { autobind } from 'core-decorators'
import { postMeal } from '_redux/modules/meals'
import { getMeal, patchMeal, clear } from '_redux/modules/meal'

import {
  TextField,
  Textarea,
  Radio,
  TextFieldArray,
  DatePicker,
  Tags
} from './Fields'
import { Row, Column } from './Flexbox'
import Nutrients from './Meals-Nutrients'

import Button from 'components/common/Button'

@asyncConnect([],
  (state) => ({
    initialValues: state.meal.data,
    loaded: state.meal.loaded
  }),
  (dispatch) => bindActionCreators({
    postMeal,
    patchMeal,
    getMeal,
    clear
  }, dispatch)
)
@reduxForm({
  form: 'meal'
})
export default class Meals extends Component {
  static propTypes = {
    location: PropTypes.object,
    handleSubmit: PropTypes.func,
    postMeal: PropTypes.func,
    dismiss: PropTypes.func,
    error: PropTypes.string,
    initialValues: PropTypes.object,
    getMeal: PropTypes.func,
    clear: PropTypes.func,
    loaded: PropTypes.bool
  }

  componentWillMount () {
    const editmeal = this.props.location.query.editmeal

    if (editmeal) {
      this.props.getMeal(parseInt(editmeal, 10))
    }
  }

  componentWillUnmount () {
    this.props.clear()
  }

  @autobind
  submitHandler (fields) {
    const {
      dismiss,
      initialValues,
      location: { query: { editmeal } }
    } = this.props

    const func = editmeal ? 'patchMeal' : 'postMeal'
    const args = editmeal ?
      { _key: initialValues._key, data: fields }
    :
      { ...fields }

    return this.props[func](args)
    .then(() => {
      dismiss()
    })
    .catch((error) => {
      throw new SubmissionError({ _error: error.message })
    })
  }

  render () {
    const {
      handleSubmit,
      error,
      loaded,
      location: {query: { editmeal }}
    } = this.props

    if (editmeal && !loaded && __CLIENT__) {
      const Spinner = require('react-spinkit')
      return (
        <Row style={{ width: '100%', justifyContent: 'center', paddingTop: 100 }}>
          <Spinner name="pacman" />
        </Row>
      )
    }

    return (
      <form
        onSubmit={handleSubmit(this.submitHandler)}
      >
        <h1>New Meal</h1>
        <Column>
          <Field
            type="text"
            name="name"
            component={TextField}
            label="Name"
          />
          <Field
            name="description"
            component={Textarea}
            label="Description"
          />
          <FieldArray
            name="tags"
            component={Tags}
            label="Tags"
          />
          <Field
            label="Meal"
            name="meal"
            component={Radio}
            options={[
              { label: 'Breakfast', value: 'breakfast' },
              { label: 'Lunch', value: 'lunch' },
              { label: 'Dinner', value: 'dinner' }
            ]}
          />
          <Field
            label="Diet"
            name="diet"
            component={Radio}
            options={[
              { label: 'Vegan', value: 'vegan' },
              { label: 'Meat', value: 'meat' }
            ]}
          />
        </Column>
        <Row>
          <Column
            style={{
              flexGrow: 1,
              width: '50%',
              paddingRight: 5
            }}
          >
            <Field
              name="date"
              component={DatePicker}
              label="Date"
            />
            <Field
              type="date"
              name="expiration"
              component={DatePicker}
              label="Best before date"
            />
            <Field
              name="storage"
              component={Textarea}
              label="Storage"
            />
            <FieldArray
              name="preperation"
              label="Preperation"
              buttonLabel="Add preperation step"
              component={TextFieldArray}
            />
            <FieldArray
              name="ingredients"
              label="Ingredients"
              buttonLabel="Add ingredient"
              component={TextFieldArray}
            />
            <FieldArray
              name="allergens"
              label="Allergens"
              buttonLabel="Add allergen"
              component={TextFieldArray}
            />
          </Column>
          <Column
            style={{
              flexGrow: 1,
              width: '50%',
              paddingLeft: 5
            }}
          >
            <Field
              type="number"
              name="calories"
              component={TextField}
              label="Calories"
              parse={Number}
            />
            <FieldArray
              name="nutrients"
              component={Nutrients}
            />
          </Column>
        </Row>
        <Row
          style={{
            justifyContent: 'flex-end',
            width: '100%'
          }}
        >
          { error &&
            <Column style={{
              color: 'red',
              justifyContent: 'center',
              alignItems: 'center',
              flexGrow: 1
            }}>
              {error}
            </Column>
          }
          <Button
            onClick={handleSubmit(this.submitHandler)}
          >
            Submit meal
          </Button>
        </Row>
      </form>
    )
  }
}
