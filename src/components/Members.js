import React, { Component } from 'react'
import { Link } from 'react-router'
import PropTypes from 'prop-types'
import { asyncConnect } from 'redux-connect'
import { bindActionCreators } from 'redux'
import moment from 'moment'
import { getMembers, deleteMember } from '_redux/modules/members'

import { Row } from './Flexbox'
import Window from './Window'
import Details from './Members-Details'

import classes from './Members.scss'
// const classes = {}

@asyncConnect([{
  promise: ({ store: { getState, dispatch }, location }) => {
    if (!getState().members.loaded) {
      return dispatch(getMembers({
        count: location.query.count || 25,
        page: location.query.page || 1
      }))
    }
  }
}],
  (state) => ({
    members: state.members.data
  }),
  (dispatch) => bindActionCreators({
    getMembers,
    deleteMember
  }, dispatch)
)
export default class Members extends Component {
  static propTypes = {
    members: PropTypes.array,
    location: PropTypes.object,
    params: PropTypes.object,
    deleteMember: PropTypes.func,
    getMembers: PropTypes.func
  }

  static defaultProps = {
    members: []
  }

  deleteMemberHandler (member) {
    const { location } = this.props
    const deleteConfirmed = confirm(`Are you sure you want to delete ${member.name}?`)

    if (deleteConfirmed) {
      this.props.deleteMember(member._key)
      .then(() => {
        this.props.getMembers({
          count: location.query.count || 25,
          page: location.query.page || 1
        })
      })
    }
  }

  render () {
    const { members, location, params } = this.props
    const { register } = location.query
    const { member } = params

    const registering = register === 'true'

    return (
      <div>
        <Row
          style={{
            justifyContent: 'space-between',
            alignItems: 'bottom',
            alignContent: 'bottom'
          }}
        >
          <h1>Members</h1>
          <Link
            style={{
              display: 'flex',
              justifyContent: 'flex-end',
              flexDirection: 'column'
            }}
            to={{
              hostname: location.hostname,
              pathname: location.pathname,
              query: {
                ...location.query,
                register: !registering
              }
            }}
          >
            Add member
          </Link>
        </Row>
        {members.map((mem, index) => (
          <div>
            <Row
              className={classes.member}
              style={{
                justifyContent: 'space-between'
              }}
              key={index}
            >
              <Link
                to={{ pathname: `/dashboard/members/${mem._key}` }}
              >
                <div>{mem.name}</div>
              </Link>
              <div>{(mem.profile && mem.profile.vegan) ? 'Vegan' : 'Regular'}</div>
              <div>Joined {moment(mem.createdAt).fromNow()}</div>
              <div
                onClick={() => this.deleteMemberHandler(mem)}
                style={{ cursor: 'pointer' }}
              >
                🚮
              </div>
            </Row>
            { (member === mem._key) &&
              <Window
                dismiss="/dashboard/members"
              >
                <Details
                  member={mem}
                  location={location}
                />
              </Window>
            }
          </div>
        ))}
      </div>
    )
  }
}
