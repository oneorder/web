import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { bindActionCreators } from 'redux'
import { asyncConnect } from 'redux-connect'
import { Link, browserHistory } from 'react-router'
import moment from 'moment'
import { autobind } from 'core-decorators'
import { getMeals } from '_redux/modules/meals'
import { deleteMeal } from '_redux/modules/meal'

import { Row } from './Flexbox'
import Window from './Window'
import Table from './common/Table'
import AddMeal from './Meals-New'
import DateChanger from './common/DateChanger'

@asyncConnect([{
  promise: ({ store: { getState, dispatch }, location }) => {
    const { count, page } = location.query

    if (!getState().meals.loaded) {
      return dispatch(getMeals({
        count,
        page
      }))
    }
  }
}],
  (state) => ({
    meals: state.meals.data
  }),
  (dispatch) => bindActionCreators({
    getMeals,
    deleteMeal
  }, dispatch)
)
export default class Meals extends Component {
  static propTypes = {
    location: PropTypes.object,
    handleSubmit: PropTypes.func,
    meals: PropTypes.array,
    deleteMeal: PropTypes.func,
    getMeals: PropTypes.func
  }

  deleteMealHandler (meal) {
    const { page, count } = this.props.location.query

    const confirmDelete = confirm(`Are you sure you'd like to delete ${meal.name}`)

    if (!confirmDelete) return

    this.props.deleteMeal(meal._key)
    .then(() => {
      this.props.getMeals({
        page,
        count
      })
    })
  }

  @autobind
  dismissModalHandler () {
    const { location } = this.props

    browserHistory.push({ ...location, query: { ...location.query, newmeal: false, editmeal: undefined } })
  }

  @autobind
  dateChangeHandler (week) {
    const { count, page } = this.props.location.query

    this.props.getMeals({
      count,
      page,
      start: moment(week).startOf('week').format(),
      end: moment(week).endOf('week').format(),
    })
  }

  render () {
    const { meals, location } = this.props
    const { query: { newmeal, editmeal } } = location

    const mealsWithTimes = meals && meals.map(meal => ({
      name: (
        <Link
          title="Edit"
          to={{ ...location, query: { ...location.query, editmeal: meal._key } }}
        >
          {meal.name}
        </Link>
      ),
      meal: meal.meal,
      diet: meal.diet,
      date: moment(meal.date).format('dddd MMMM DD, YYYY'),
      delete: (
        <div
          onClick={() => this.deleteMealHandler(meal)}
          title="delete"
          style={{
            cursor: 'pointer'
          }}
        >
          🚮
        </div>
      )
    }))

    const style = { flex: 1, width: undefined }

    return (
      <div>
        { (newmeal === 'true' || editmeal) &&
          <Window
            dismiss={this.dismissModalHandler}
            style={{ width: 700, height: '75vh' }}
          >
            <AddMeal
              dismiss={this.dismissModalHandler}
              location={location}
            />
          </Window>
        }
        <Row
          style={{
            justifyContent: 'space-between'
          }}
        >
          <h1>Meals</h1>
          <DateChanger
            unit="week"
            label="WEEK OF"
            onChange={this.dateChangeHandler}
            location={location}
          />
          <Link
            to={{
              ...location,
              query: { ...location.query, newmeal: true }
            }}
          >
            New meal
          </Link>
        </Row>
        { meals &&
          <Table
            columns={[
              { name: 'name', style: { ...style, flex: 2 } },
              { name: 'date', style: { ...style, flex: 2 } },
              { name: 'meal', style },
              { name: 'diet', style },
              { name: 'delete', style }
            ]}
            data={mealsWithTimes}
          />
        }
      </div>
    )
  }
}
