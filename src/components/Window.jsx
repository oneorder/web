import React, {Component} from 'react'
import PropTypes from 'prop-types'
import { browserHistory } from 'react-router'
import Modal from 'react-dumb-modal'

import { Row } from './Flexbox'

import classes from './Window.scss'
// const classes = {}

export default class Window extends Component {
  static propTypes = {
    children: PropTypes.element,
    params: PropTypes.object,
    className: PropTypes.string,
    overlayClassName: PropTypes.string,
    location: PropTypes.object,
    dismiss: PropTypes.oneOfType([
      PropTypes.func,
      PropTypes.string,
      PropTypes.object
    ]),
    style: PropTypes.object,
    hideBack: PropTypes.bool
  }

  dismissModalHandler () {
    const { dismiss } = this.props

    if (!dismiss && this.props.location) {
      return browserHistory.push(this.props.location.pathname)
    }

    if (!dismiss || typeof dismiss === 'string') {
      browserHistory.push(dismiss || '/')
    }

    if (typeof dismiss === 'object') {
      return browserHistory.push(dismiss)
    }

    dismiss()
  }

  render () {
    const { hideBack, style } = this.props

    return (
      <Modal
        overlayClassName={ this.props.overlayClassName || classes.defaultModalOverlay }
        modalClassName={ this.props.className || classes.defaultModal }
        overlayStyle={{}}
        modalStyle={style || {}}
      >
        {!hideBack &&
          <Row
            style={{
              backgroundColor: 'rgba(255,255,255,0.8)',
              width: '100%',
              position: 'fixed',
              zIndex: 50,
              top: 0,
              height: 39,
              left: 20,
              alignItems: 'center'
            }}
          >
            <div
              onClick={::this.dismissModalHandler}
              style={{
                cursor: 'pointer',
                fontSize: 20,
                fontFamily: 'Futura Condensed Bold'
              }}
            >
              {'< Back'}
            </div>
          </Row>
        }
        { React.cloneElement(this.props.children, {
          params: this.props.params,
          dismiss: ::this.dismissModalHandler
        })}
      </Modal>
    )
  }
}
