import React from 'react'
import { Route } from 'react-router'

import Addresses from 'components/Addresses'
import AppLayout from 'components/AppLayout'
import Meals from 'components/Meals'
import Members from 'components/Members'
import Sidebar from 'components/Sidebar'
import Orders from 'components/Orders'
import Kitchen from 'components/Orders-Kitchen'
import Deliveries from 'components/Orders-Deliveries'
import NewOrder from 'components/Orders-New'
import Complete from 'components/Orders-New-Complete'
import Profile from 'components/Profile'

export default () => {
  return (
    <Route path="/" component={AppLayout}>
      <Route path="order" component={NewOrder} />
      <Route path="/order/done" component={Complete} />
      <Route path="/profile" component={Profile} />
      <Route path="/dashboard" component={Sidebar}>
        <Route path="addresses" component={Addresses} />
        <Route path="meals" component={Meals} />
        <Route path="members" component={Members} />
        <Route path="members/:member" component={Members} />
        <Route component={Orders}>
          <Route path="deliveries" component={Deliveries} />
          <Route path="kitchen" component={Kitchen} />
        </Route>
      </Route>
    </Route>
  );
}
