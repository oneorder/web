/* Eris Rassumsen */

import superagent from 'superagent';
import config from '../config';

const methods = ['get', 'post', 'put', 'patch', 'del'];

function formatUrl (path) {
  const adjustedPath = path[0] !== '/' ? '/' + path : path;
  return config.apiHost + adjustedPath;
}

export default class ApiClient {
  constructor(req) {
    methods.forEach((method) =>
      this[method] = (path, { query, data, headers } = {}) => new Promise((resolve, reject) => {
        const url = formatUrl(path)
        const request = superagent[method](url)

        if (query) {
          request.query(query);
        }

        if (window.localStorage.authToken) {
          request.set({ Authorization: window.localStorage.authToken })
        }

        if (headers) {
          request.set(headers)
        }

        if (__SERVER__ && req.get('cookie')) {
          request.set('cookie', req.get('cookie'));
        }

        if (data) {
          request.send(data);
        }

        request.end((err, { body } = {}) => err ? reject(body || err) : resolve(body));
      }));
  }
  /*
   * There's a V8 bug where, when using Babel, exporting classes with only
   * constructors sometimes fails. Until it's patched, this is a solution to
   * "ApiClient is not defined" from issue #14.
   * https://github.com/erikras/react-redux-universal-hot-example/issues/14
   *
   * Relevant Babel bug (but they claim it's V8): https://phabricator.babeljs.io/T2455
   *
   * Remove it at your own risk.
   */
  empty() {}
}
